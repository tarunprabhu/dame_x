#ifndef TLP_DATATYPES_MYJIT_FUNCTION_H
#define TLP_DATATYPES_MYJIT_FUNCTION_H

#include <set>
#include <string>
#include <vector>

#include <stdint.h>

#include "types.h"
#include "values.h"

extern "C" {
#include "xed/xed-interface.h"
#include "xed/xed-portability.h"
}

// Forward declaration to prevent circular include dependencies
class BasicBlock;
class Constant;

class Function {
protected:
    std::string _name;
    // This is the buffer where the compiled function is
    std::vector<byte> opcodes;
    // We're going to have all the functions in a single large buffer, so this
    // is the displacement of the function from the start of the buffer
    unsigned pos;
    std::vector<BasicBlock*> blocks;
    std::set<Function*> callees;
    std::vector<const BasicBlock*> jumptable;
    std::vector<Function*> _buildorder;
public:
    Function(std::string name) 
        : _name(name), opcodes(decltype(opcodes)()), pos(0),
          blocks(decltype(blocks)()), callees(decltype(callees)()),
          jumptable(decltype(jumptable)()),
          _buildorder(decltype(_buildorder)()) {
        ;
    }
    virtual ~Function();

    std::string name()     const { return _name; }
    unsigned size()        const { return opcodes.size() + jumptable.size()*8; }
    unsigned entry()       const { return pos; }
    void copy(byte *d)     const;
    std::vector<Function*> buildorder() const { return _buildorder; }

    int indexOf(const BasicBlock*) const;
    void insertBefore(BasicBlock *n, BasicBlock *loc=nullptr);
    void addCallee(Function *fn) { callees.insert(fn); }
    void constructBuildOrder(std::vector<Function*> &order);

    bool jtadd(const BasicBlock *bb, unsigned idx);
    unsigned jtexpand(unsigned count);
    unsigned jtcurr()                        const { return jumptable.size(); }
    
    virtual void printPtr(std::ostream &, byte*) const;
    virtual bool encode();
    virtual bool pass1(unsigned&);
    virtual bool pass2(std::vector<byte>&, unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

// This is used to represent functions like memcpy. We use it to make it 
// easier while doing code generation
class LibraryFunction : public Function {
protected:
    Constant *_addr;
public:
    LibraryFunction(std::string name, void *ptr);
    virtual ~LibraryFunction();

    void insertBefore(BasicBlock *n, BasicBlock *loc=nullptr) { ; }
    Constant* addr() const { return _addr; }

    virtual void printPtr(std::ostream&, byte*) const;
    virtual bool encode();
    virtual bool pass1(unsigned&);
    virtual bool pass2(std::vector<byte>&, unsigned&);
    virtual void gas(std::ostream&) const;
};

#endif
