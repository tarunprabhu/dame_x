#include "instruction.h"

#include "function.h"
#include "basicblock.h"
#include "types.h"
#include "util.h"

#include <assert.h>
#include <iomanip>
#include <iostream>
#include <sstream>

using namespace std;

#define stream_mnemonic(os, mnemonic)                               \
    os << left << setw(10) << setfill(' ') << (mnemonic) << " ";    \

static void init_encoder_request(xed_encoder_request_t &req, unsigned width=64) {
    xed_state_t state;
    state.mmode = XED_MACHINE_MODE_LONG_64;
    state.stack_addr_width = XED_ADDRESS_WIDTH_32b;

    xed_encoder_request_zero_set_mode(&req, &state);
    xed_encoder_request_zero_operand_order(&req);
    xed_encoder_request_set_effective_address_size(&req, 64);
    xed_encoder_request_set_effective_operand_width(&req, width);
    xed_encoder_request_set_memory_operand_length(&req, width/8);
}

static EncodeMetadata get_encode_metadata(bool islookup=true) {
    return { 0, 0, 0, islookup };
}

bool Instruction::do_encode(xed_encoder_request_t &req, unsigned &curr) {
    xed_error_enum_t err = xed_encode(&req, opcodes, XED_MAX_INSTRUCTION_BYTES, &bytes);
    if(err != XED_ERROR_NONE) {
        stringstream ss;
        ss << xed_error_enum_t2str(err);
        dljit_error(ss.str());
        return false;
    }
    curr += bytes;
    return true;
}

void Instruction::do_print(ostream &os, unsigned &curr) const {
    os << "  " << right << setfill(' ') << setw(6) << hex << curr << ":    ";
    for(unsigned i = 0; i < XED_MAX_INSTRUCTION_BYTES; i++)
        if(i < bytes)
            os << setfill('0') << setw(2) << hex << (int)opcodes[i] << " ";
        else
            os << "  " << " ";
    os << "    ";
    curr += bytes;
}

bool Instruction::do_copy(vector<byte> &buf, unsigned &pos) {
    for(unsigned i = 0; i < bytes; i++)
        buf.at(pos+i) = opcodes[i];
    pos += bytes;
    return true;
}

bool Instruction::pass2(vector<byte> &buf, unsigned &pos) {
    return do_copy(buf, pos);
}

bool BranchInst::pass1(unsigned &curr) {
    // If this is an unconditional jump to the immediate successor block,
    // just ignore it
    if(_type == MP) {
        const BasicBlock *curr = parent();
        const Function *f = curr->parent();
        if(f->indexOf(_target) == (f->indexOf(curr)+1))
            return true;
    }

    xed_encoder_request_t req;
    init_encoder_request(req);

    switch(_type) {
    case MP: xed_encoder_request_set_iclass(&req, XED_ICLASS_JMP);  break;
    case GT: xed_encoder_request_set_iclass(&req, XED_ICLASS_JNLE); break;
    case GE: xed_encoder_request_set_iclass(&req, XED_ICLASS_JNL);  break;
    case LT: xed_encoder_request_set_iclass(&req, XED_ICLASS_JL);   break;
    case LE: xed_encoder_request_set_iclass(&req, XED_ICLASS_JLE);  break;
    case EQ: xed_encoder_request_set_iclass(&req, XED_ICLASS_JZ);   break;
    case NE: xed_encoder_request_set_iclass(&req, XED_ICLASS_JNZ);  break;
    case Z:  xed_encoder_request_set_iclass(&req, XED_ICLASS_JZ);   break;
    case NZ: xed_encoder_request_set_iclass(&req, XED_ICLASS_JNZ);  break;
    default: assert(0 && "encode: unknown branch type"); break;
    }

    if(_target->isaddrset()) {
        Displacement(_target->addr()-curr-6).encode(req);
    } else {
        Displacement(0xbabe007).encode(req);
    }
    
    return do_encode(req, curr);
}

bool BranchInst::pass2(vector<byte> &buf, unsigned &pos) {
    int disp = _target->addr()-(pos+bytes);
    for(int i = bytes-4; i < (int)bytes; i++, disp >>= 8)
        opcodes[i] = disp & 0xff;

    return do_copy(buf, pos);
}

void BranchInst::gas(ostream &os, unsigned &idx, int opt) const {
    // If this is an unconditional jump to the immediate successor block,
    // just ignore it
    if(_type == MP) {
        const BasicBlock *curr = parent();
        const Function *f = curr->parent();
        if(f->indexOf(_target) == (f->indexOf(curr)+1))
            return;
    }
    if(opt == 0)
        do_print(os, idx);
    switch(_type) {
    case MP: stream_mnemonic(os, "jmp"); break;
    case GT: stream_mnemonic(os, "jg");  break;
    case GE: stream_mnemonic(os, "jge"); break;
    case LT: stream_mnemonic(os, "jl"); break;
    case LE: stream_mnemonic(os, "jle"); break;
    case EQ: stream_mnemonic(os, "jz");  break;
    case NE: stream_mnemonic(os, "jnz"); break;
    case Z:  stream_mnemonic(os, "jz");  break;
    case NZ: stream_mnemonic(os, "jnz"); break;
    default: assert(0 && "gas: unknown branch type"); break;
    }
    if(opt == 0)   
        os << _target->addr() << " <" << _target->name() << ">" << endl;
    else
        os << _target->name() << endl;
}

bool CondMoveInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req, _src->width());

    switch(_type) {
    case GT: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVNLE); break;
    case GE: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVNL);  break;
    case LT: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVL);   break;
    case LE: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVLE);  break;
    case EQ: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVZ);   break;
    case NE: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVNZ);  break;
    case Z:  xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVZ);   break;
    case NZ: xed_encoder_request_set_iclass(&req, XED_ICLASS_CMOVNZ);  break;
    default: assert(0 && "encode: unknown conditional move type"); break;
    }

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata, 32);
    
    return do_encode(req, curr);
}

void CondMoveInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    switch(_type) {
    case GT: stream_mnemonic(os, "cmovg");  break;
    case GE: stream_mnemonic(os, "cmovge"); break;
    case LT: stream_mnemonic(os, "cmovlt"); break;
    case LE: stream_mnemonic(os, "cmovle"); break;
    case EQ: stream_mnemonic(os, "cmovz");  break;
    case NE: stream_mnemonic(os, "cmovnz"); break;
    case Z:  stream_mnemonic(os, "cmovz");  break;
    case NZ: stream_mnemonic(os, "cmovnz"); break;
    default: assert(0 && "gas: unknown conditional move type"); break;
    }
    os << _src->gas() << ", " << _dst->gas() << endl;
}


bool CmpInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req, _rhs->width());

    xed_encoder_request_set_iclass(&req, XED_ICLASS_CMP);

    EncodeMetadata metadata = get_encode_metadata();
    _lhs->encode(req, metadata);
    _rhs->encode(req, metadata);

    return do_encode(req, curr);
}

void CmpInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);

    stream_mnemonic(os, "cmpq");
    os << _lhs->gas() << ", " << _rhs->gas() << endl;
}


bool LoadInst::pass1(unsigned &curr) { 
    xed_encoder_request_t req;
    init_encoder_request(req, _dst->width());

    xed_encoder_request_set_iclass(&req, XED_ICLASS_MOV);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata);

    return do_encode(req, curr);
}

void LoadInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    switch(_dst->width()) {
    case 64: stream_mnemonic(os, "movq"); break;
    case 32: stream_mnemonic(os, "movd"); break;
    case 16: stream_mnemonic(os, "movl"); break;
    case 8: stream_mnemonic(os, "mov"); break;
    }
    os << _src->gas() << ", " << _dst->gas() << endl;
}


bool StoreInst::pass1(unsigned &curr) { 
    xed_encoder_request_t req;
    init_encoder_request(req, _src->width());

    xed_encoder_request_set_iclass(&req, XED_ICLASS_MOV);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata, 32);

    return do_encode(req, curr);
}

void StoreInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    switch(_dst->width()) {
    case 64: stream_mnemonic(os, "movq"); break;
    case 32: stream_mnemonic(os, "movd"); break;
    case 16: stream_mnemonic(os, "movl"); break;
    case 8: stream_mnemonic(os, "mov"); break;
    }
    os << _src->gas() << ", " << _dst->gas() << endl;
}

bool MoveInst::pass1(unsigned &curr) { 
    xed_encoder_request_t req;
    init_encoder_request(req, _src->width());

    xed_encoder_request_set_iclass(&req, XED_ICLASS_MOV);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata, 32);

    return do_encode(req, curr);
}

bool MoveInst::pass2(vector<byte> &buf, unsigned &curr) {
    // The second time around, the blockaddress will actually be set, so we
    // reencode the 
    if(const BlockAddress *b = dynamic_cast<const BlockAddress*>(_src)) {
        unsigned c = b->addr();
        for(unsigned i = bytes-4; i < bytes; i++, c >>= 8)
            opcodes[i] = c & 0xff;
    }
    return do_copy(buf, curr);
}

void MoveInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "movq");
    os << _src->gas() << ", " << _dst->gas() << endl;
}

bool MoveSignExtendInst::pass1(unsigned &curr) {
  xed_encoder_request_t req;
  init_encoder_request(req);

  xed_encoder_request_set_iclass(&req, XED_ICLASS_MOVSXD);

  EncodeMetadata metadata = get_encode_metadata();
  _dst->encode(req, metadata);
  _src->encode(req, metadata);

  return do_encode(req, curr);
}

void MoveSignExtendInst::gas(ostream &os, unsigned &idx, int opt) const {
  if(opt == 0)
    do_print(os, idx);
  stream_mnemonic(os, "movsx");
  os << _src->gas() << ", " << _dst->gas() << endl;
}

bool PtrInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_LEA);

    EncodeMetadata metadata = get_encode_metadata(false);
    _dst->encode(req, metadata);
    _src->encode(req, metadata);

    return do_encode(req, curr);
}

void PtrInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "leaq");
    os <<  _src->gas() << ", " << _dst->gas() << endl;
}


bool PushInst::pass1(unsigned &curr) { 
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_PUSH);

    _reg->encode(req);

    return do_encode(req, curr);
}

void PushInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "pushq");
    os << _reg->gas() << endl;
}


bool PopInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_POP);

    _reg->encode(req);

    return do_encode(req, curr);
}

void PopInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "popq");
    os << _reg->gas() << endl;
}


bool CallInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_CALL_NEAR);

    Displacement(_f->entry()-(curr+5)).encode(req);

    return do_encode(req, curr);
}

bool CallInst::pass2(vector<byte> &buf, unsigned &pos) {
    // This will always have a 32-bit displacement so it's a 5-byte instruction
    int disp = _f->entry()-(pos+5);
    for(unsigned i = 1; i < 5; i++, disp >>= 8)
        opcodes[i] = disp & 0xff;
    return do_copy(buf, pos);
}

void CallInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "callq");
    if(opt == 0)   
        os << _f->entry() << " <" << _f->name() << ">" << endl;
    else
        os << _f->name() << endl;
}


bool IndirectBrInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_JMP);

    _reg->encode(req);

    return do_encode(req, curr);
}

void IndirectBrInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "jmpq");
    os << "*" << _reg->gas() << endl;
}


bool IndirectCallInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_CALL_NEAR);

    _reg->encode(req);

    return do_encode(req, curr);
}

void IndirectCallInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "call");
    os << "*" << _reg->gas() << endl;
}

bool BinaryInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    unsigned width = _op1->width();
    if(_op2->width() > _op1->width())
        width = _op2->width();
    init_encoder_request(req, width);

    switch(_type) {
    case Add:  xed_encoder_request_set_iclass(&req, XED_ICLASS_ADD);  break;
    case Sub:  xed_encoder_request_set_iclass(&req, XED_ICLASS_SUB);  break;
    case And:  xed_encoder_request_set_iclass(&req, XED_ICLASS_AND);  break;
    case Or:   xed_encoder_request_set_iclass(&req, XED_ICLASS_OR);   break;
    case Xor:  xed_encoder_request_set_iclass(&req, XED_ICLASS_XOR);  break;
    case Test: xed_encoder_request_set_iclass(&req, XED_ICLASS_TEST); break;
    default: assert(0 && "encode; Unknown binary instruction type");  break;
    }

    EncodeMetadata metadata = get_encode_metadata();
    _op1->encode(req, metadata, 8);
    _op2->encode(req, metadata, 8);

    return do_encode(req, curr);
}

void BinaryInst::gas(ostream& os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    switch(_type) {
    case BinaryType::Add:  stream_mnemonic(os, "addq"); break; 
    case BinaryType::Sub:  stream_mnemonic(os, "subq"); break;
    case BinaryType::And:  stream_mnemonic(os, "andq"); break;
    case BinaryType::Or:   stream_mnemonic(os, "orq");  break;
    case BinaryType::Xor:  stream_mnemonic(os, "xorq"); break;
    case BinaryType::Test: stream_mnemonic(os, "testq"); break;
    default: assert(0 && "nasm: Unknown binary instruction type"); break;
    }
    os << _op2->gas() << ", " << _op1->gas() << endl;
}


bool UnaryInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);

    switch(_type) {
    case Inc: xed_encoder_request_set_iclass(&req, XED_ICLASS_INC); break;
    case Dec: xed_encoder_request_set_iclass(&req, XED_ICLASS_DEC); break;
    case Not: xed_encoder_request_set_iclass(&req, XED_ICLASS_NOT); break;
    case Mul: xed_encoder_request_set_iclass(&req, XED_ICLASS_MUL); break;
    case Div: xed_encoder_request_set_iclass(&req, XED_ICLASS_DIV); break;
    case Rem: xed_encoder_request_set_iclass(&req, XED_ICLASS_DIV); break;
    default: assert(0 && "encode: unknown unary instruction type"); break;
    }

    _op->encode(req, 32);

    return do_encode(req, curr);
}

void UnaryInst::gas(ostream& os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    switch(_type) {
    case UnaryType::Inc: stream_mnemonic(os, "incq"); break;
    case UnaryType::Dec: stream_mnemonic(os, "decq"); break;
    case UnaryType::Not: stream_mnemonic(os, "notq"); break;
    case UnaryType::Mul: stream_mnemonic(os, "mulq"); break;
    case UnaryType::Div: stream_mnemonic(os, "divq"); break;
    case UnaryType::Rem: stream_mnemonic(os, "divq"); break;
    default: assert(0 && "gas: unknown unary instruction type"); break;
    }
    os << _op->gas() << endl;
}


bool ReturnInst::pass1(unsigned &curr) { 
    xed_encoder_request_t req;
    init_encoder_request(req);

    xed_encoder_request_set_iclass(&req, XED_ICLASS_RET_NEAR);

    return do_encode(req, curr);
}

void ReturnInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, "retq");
    os << endl;
}

bool SSELoadInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);
    xed_encoder_request_set_memory_operand_length(&req, 16);
    
    if(_aligned)
        xed_encoder_request_set_iclass(&req, XED_ICLASS_MOVAPS);
    else
        xed_encoder_request_set_iclass(&req, XED_ICLASS_MOVUPS);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata);

    return do_encode(req, curr);
}

void SSELoadInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, (_aligned ? "movaps" : "movups"));
    os << _src->gas() << ", " << _dst->gas() << endl;
}

bool SSEStoreInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);
    xed_encoder_request_set_memory_operand_length(&req, 16);

    if(_aligned)
        xed_encoder_request_set_iclass(&req, XED_ICLASS_MOVAPS);
    else
        xed_encoder_request_set_iclass(&req, XED_ICLASS_MOVUPS);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata);

    return do_encode(req, curr);
}

void SSEStoreInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, (_aligned ? "movaps" : "movups"));
    os << _src->gas() << ", " << _dst->gas() << endl;
}

bool AVXLoadInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);
    xed_encoder_request_set_memory_operand_length(&req, 32);

    if(_aligned)
        xed_encoder_request_set_iclass(&req, XED_ICLASS_VMOVAPS);
    else
        xed_encoder_request_set_iclass(&req, XED_ICLASS_VMOVUPS);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata);

    return do_encode(req, curr);
}

void AVXLoadInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, (_aligned ? "vmovaps" : "vmovups"));
    os << _src->gas() << ", " << _dst->gas() << endl;
}

bool AVXStoreInst::pass1(unsigned &curr) {
    xed_encoder_request_t req;
    init_encoder_request(req);
    xed_encoder_request_set_memory_operand_length(&req, 32);

    if(_aligned)
        xed_encoder_request_set_iclass(&req, XED_ICLASS_VMOVAPS);
    else
        xed_encoder_request_set_iclass(&req, XED_ICLASS_VMOVUPS);

    EncodeMetadata metadata = get_encode_metadata();
    _dst->encode(req, metadata);
    _src->encode(req, metadata);

    return do_encode(req, curr);
}

void AVXStoreInst::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0)
        do_print(os, idx);
    stream_mnemonic(os, (_aligned ? "vmovaps" : "vmovups"));
    os << _src->gas() << ", " << _dst->gas() << endl;
}
