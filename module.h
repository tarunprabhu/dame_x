#ifndef TLP_DATATYPES_MYJIT_MODULE_H
#define TLP_DATATYPES_MYJIT_MODULE_H

#include <map>
#include <string>

#include "function.h"

class Module { 
private:
    std::map<std::string, Function*> functions;
public:
    Module()
        : functions(decltype(functions)()) {
        ;
    }
    ~Module();

    Function* getFunction(std::string name) const;
    LibraryFunction* getLibraryFunction(std::string name, void *ptr);
    Function* getOrInsertFunction(std::string name);

    void gas(std::ostream&, int=0) const;
};


#endif
