#ifndef TLP_DATATYPES_MYJIT_DLJIT_H
#define TLP_DATATYPES_MYJIT_DLJIT_H

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C
#endif

#include "types.h"
  
EXTERN_C void DLJIT_initialize();
EXTERN_C void DLJIT_finalize();

EXTERN_C void* DLJIT_compile(void*);
EXTERN_C DLJIT_function_ptr DLJIT_function_pack(void*);
EXTERN_C DLJIT_function_ptr DLJIT_function_unpack(void*);
EXTERN_C void DLJIT_free(void*);
    
#endif
