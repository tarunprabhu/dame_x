#ifndef TLP_DATATYPES_MYJIT_DEBUG_H
#define TLP_DATATYPES_MYJIT_DEBUG_H

#define DEBUG_PUTS(env, _s)                                             \
    {                                                                   \
        Builder& builder = (env).builder();                             \
        Module& module = (env).module();                                \
        const char *s = _s;                                             \
        const char *fmt = "%s\x0A";                                     \
        const Constant *aFmt = (env).int64Const((long)fmt);             \
        const Constant *aS = (env).int64Const((long)s);                 \
        const Register *rax = builder.ret();                            \
        auto printfFn = module.getLibraryFunction("printf", (void*)printf); \
        builder.CreateXor(rax, rax, rax);                               \
        builder.CreateCall(printfFn, aFmt, aS);                         \
    }                                                                   \
    
#define DEBUG_PRINTF(env, _s, _reg)                                     \
    {                                                                   \
        Builder& builder = (env).builder();                             \
        Module& module = (env).module();                                \
        const char *s = _s;                                             \
        const char *fmt = "%s: %ld\x0A";                                \
        const Register *reg = (_reg);                                   \
        const Constant *aFmt = (env).int64Const((long)fmt);             \
        const Constant *aName = (env).int64Const((long)s);              \
        auto printfFn = module.getLibraryFunction("printf", (void*)printf); \
        const Register *rax = builder.ret();                            \
        if(reg != rax) {                                                \
            builder.CreateXor(rax, rax, rax);                           \
            (builder).CreateCall(printfFn, aFmt, aName, reg);           \
        } else {                                                        \
            const Register *tmp = builder.getRegister();                \
            builder.CreateMove(tmp, reg);                               \
            builder.CreateXor(rax, rax, rax);                           \
            builder.CreateCall(printfFn, aFmt, aName, tmp);             \
            builder.CreateMove(reg, tmp);                               \
            builder.clearRegister(tmp);                                 \
        }                                                               \
    }                                                                   \

#define DEBUG_PRINTF_HEX(env, _s, _reg)                                 \
    {                                                                   \
        Builder &builder = (env).builder();                             \
        Module& module = (env).module();                                \
        const char *s = _s;                                             \
        const char *fmt = "%s: 0x%x\x0A";                               \
        const Register *reg = (_reg);                                   \
        const Constant *aFmt = env.int64Const((long)fmt);               \
        const Constant *aName = env.int64Const((long)s);                \
        auto printfFn = module.getLibraryFunction("printf", (void*)printf); \
        const Register *rax = builder.ret();                            \
        if(reg != rax) {                                                \
            builder.CreateXor(rax, rax, rax);                           \
            (builder).CreateCall(printfFn, aFmt, aName, reg);           \
        } else {                                                        \
            const Register *tmp = builder.getRegister();                \
            builder.CreateMove(tmp, reg);                               \
            builder.CreateXor(rax, rax, rax);                           \
            builder.CreateCall(printfFn, aFmt, aName, tmp);             \
            builder.CreateMove(reg, tmp);                               \
            builder.clearRegister(tmp);                                 \
        }                                                               \
    }                                                                   \


#define DEBUG_PRINTF3_HEX(env, _s, _reg0, _reg1, _reg2)                 \
    {                                                                   \
        Builder& builder = (env).builder();                             \
        Module& module = (env).module();                                \
        const char *s = _s;                                             \
        const char *fmt = "%s: 0x%x\t0x%x\t0x%x\x0A";                   \
        const Register *reg0 = (_reg0);                                 \
        const Register *reg1 = (_reg1);                                 \
        const Register *reg2 = (_reg2);                                 \
        const Constant *aFmt = env.int64Const((long)fmt);               \
        const Constant *aName = env.int64Const((long)s);                \
        auto printfFn = module.getLibraryFunction("printf", (void*)printf); \
        const Register *rax = builder.ret();                            \
        builder.CreateXor(rax, rax, rax);                               \
        builder.CreateCall(printfFn, aFmt, aName, reg0, reg1, reg2);    \
    }                                                                   \


#endif
