#include "jit.h"

#include <sstream>

using namespace std;


#ifdef DEBUG
const char *blkidx_pushes[] = { "blkidx0_push", "blkidx1_push", "blkidx2_push",
                             "blkidx3_push", "blkidx4_push", "blkidx5_push",
                             "blkidx6_push", "blkidx7_push", "blkidx8_push",
                             "blkidx9_push", "blkidx10_push", "blkidx11_push",
                             "blkidx12_push", "blkidx13_push", "blkidx14_push"};
const char *blkidx_pops[] = { "blkidx0_pop", "blkidx1_pop", "blkidx2_pop",
                           "blkidx3_pop", "blkidx4_pop", "blkidx5_pop",
                           "blkidx6_pop", "blkidx7_pop", "blkidx8_pop",
                           "blkidx9_pop", "blkidx10_pop", "blkidx11_pop",
                           "blkidx12_pop", "blkidx13_pop", "blkidx14_pop" };

const char *bf_pushes[] = { "bf0_push", "bf1_push", "bf2_push",
                             "bf3_push", "bf4_push", "bf5_push",
                             "bf6_push", "bf7_push", "bf8_push",
                             "bf9_push", "bf10_push", "bf11_push",
                             "bf12_push", "bf13_push", "bf14_push" };
const char *bf_pops[] = { "bf0_pop", "bf1_pop", "bf2_pop",
                           "bf3_pop", "bf4_pop", "bf5_pop",
                           "bf6_pop", "bf7_pop", "bf8_pop",
                           "bf9_pop", "bf10_pop", "bf11_pop",
                           "bf12_pop", "bf13_pop", "bf14_pop" };
#endif


static BasicBlock* DLJIT_blkidx_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();

    long offset0 = dl[sp].s.bi_t.offsets[0];

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBlklen = env.int64Const(dl[sp].s.bi_t.blklen);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
        
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "blkidx_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, blkidx_pushes[sp]);
    }
#endif
    if(pack) {
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
    } else {
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
    }
    builder.CreateStore(llvmCount, stack, disp_stack(sp, countLeft));
    
    if(dl[sp].kind != DL_BLOCKINDEX1) {
        if(pack) {
            builder.CreateStore(inbase, stack, disp_stack(sp+1, base));
        } else {
            builder.CreateStore(outbase, stack, disp_stack(sp+1, base));
        }
        builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    }
    
    // inbase/outbase += dl[sp].s.bi_t.offsets[0]
    if(pack) {
        builder.CreateAdd(inbase, inbase, env.int64Const(offset0));
    } else {
        builder.CreateAdd(outbase, outbase, env.int64Const(offset0));
    }

    builder.CreateBr(bbInner);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "blkidx_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

static BasicBlock* DLJIT_blkidx_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    stringstream ss;

    ss.str(string());
    ss << "blkidx" << sp << "_left";
    BasicBlock *bbLeft = new BasicBlock(ss.str(), fn, bbIns);

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBlklen = env.int64Const(dl[sp].s.bi_t.blklen);
    const Constant *llvmOffsetPtr = env.int64Const((long)dl[sp].s.bi_t.offsets);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
    
    // if(stack[sp].countLeft == 0) goto bbOuter;
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "blkidx_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, blkidx_pops[sp]);
    }
#endif
    const Register *left = builder.getRegister();
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbOuter, bbLeft);

    // Not done
    builder.SetInsertPoint(bbLeft);
    const Register *base = builder.getRegister();
    const Register *idx = builder.getRegister();
    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();

    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateSub(idx, llvmCount, left);
    builder.CreateMove(offsetsR, llvmOffsetPtr);
    builder.CreateLoad(offset, offsetsR, 0, idx, 8);
    if(pack) {
        builder.CreatePtr(inbase, base, 0, offset);
    } else {
        builder.CreatePtr(outbase, base, 0, offset);
    }

    if(dl[sp].kind != DL_BLOCKINDEX1) {
        // stack[sp+1].countLeft = dl[sp].s.bi_t.blklen
        builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    }
    
    builder.CreateBr(bbInner);

    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);
    builder.clearRegister(idx);
    builder.clearRegister(base);
    builder.clearRegister(left);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "blkidx_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_blockindexed(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();
    stringstream ss;

    ss.str(string());
    ss << "blkidx" << sp << "_pop";
    BasicBlock *bbPop = new BasicBlock(ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(string());
    ss << "blkidx" << sp << "_push";
    BasicBlock *bbPush = new BasicBlock(ss.str(), fn, bbInner);

    auto rvPop = DLJIT_blkidx_pop(env, sp, bbPop, bbOuter, bbInner, bbIns,pack);
    auto rvPush = DLJIT_blkidx_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

static BasicBlock* DLJIT_blkfinal_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    
    long size = dl[sp].size;
    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long blklen = dl[sp].s.bi_t.blklen;
    long oldsize = dl[sp].s.bi_t.oldsize;
    void* offsets = (void*)dl[sp].s.bi_t.offsets;
    long blksize = blklen * oldsize;

    const Constant *llvmSize = env.int64Const(size);
    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmOffsets = env.int64Const((long)offsets);
    const Constant *llvmBlklen = env.int64Const(blklen);
    const Constant *llvmBlksize = env.int64Const(blksize);

    Function *packFn = DLJIT_gen_blkidxfinal(env, offsets, blklen, oldsize, extent, flags, pack);

    auto bbAll = new BasicBlock("bf_push_all", fn, bbIns);
    auto bbCount = new BasicBlock("bf_push_count", fn, bbIns);
    auto bbSome = new BasicBlock("bf_push_some", fn, bbIns);
    auto bbNone = new BasicBlock("bf_push_none", fn, bbIns);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *stack = env.stack();

    /* Entry */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "bf_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, bf_pushes[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *left = builder.getRegister();
    if(pack) {
        builder.CreateMove(base, inbase);
    } else {
        builder.CreateMove(base, outbase);
    }
    builder.CreateMove(left, llvmCount);    
    builder.CreateStore(base, stack, disp_stack(sp, base));
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(builder.CreateICmpGE(sizeleft, llvmSize),
                          bbAll, bbCount);

    /* All */
    builder.SetInsertPoint(bbAll);
    if(pack) {
        builder.CreateCall(packFn, outbase, base, env.int64Const(0), llvmCount);
        builder.CreateMove(outbase, builder.ret());
        builder.CreateAdd(inbase, base, llvmExtent);
    } else {
        builder.CreateCall(packFn, base, inbase, env.int64Const(0), llvmCount);
        builder.CreateMove(inbase, builder.ret());
        builder.CreateAdd(outbase, base, llvmExtent);
    }
    builder.CreateStore(env.int64Const(0), stack, disp_stack(sp, countLeft));
    builder.CreateSub(sizeleft, sizeleft, llvmSize);    
    builder.CreateBr(bbOuter);

    const Register *ct = builder.getRegister();
    builder.SetInsertPoint(bbCount);
    builder.CreateDiv(ct, sizeleft, llvmBlksize);
    builder.CreateCondBr(builder.CreateICmpGT(ct, env.int64Const(0)),
                          bbSome, bbNone);

    /* Some */
    builder.SetInsertPoint(bbSome);
    if(pack) {
        builder.CreateCall(packFn, outbase, base, env.int64Const(0), ct);
        builder.CreateMove(outbase, builder.ret());
    } else {
        builder.CreateCall(packFn, base, inbase, env.int64Const(0), ct);
        builder.CreateMove(inbase, builder.ret());
    }
    const Register *tmp = builder.getRegister();
    builder.CreateMul(tmp, ct, llvmBlksize);
    builder.CreateSub(sizeleft, sizeleft, tmp);
    builder.clearRegister(tmp);
    builder.CreateSub(left, left, ct);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateBr(bbNone);
    
    /* None */
    builder.SetInsertPoint(bbNone);
    const Register *idx = ct;
    builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateLoad(offset, offsetsR, 0, idx, 8);
    if(pack) {
        builder.CreatePtr(inbase, base, 0, offset);
    } else {
        builder.CreatePtr(outbase, base, 0, offset);
    }
    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);
    // goto dlpush
    builder.CreateBr(bbInner);

    builder.clearRegister(ct);
    builder.clearRegister(left);
    builder.clearRegister(base);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "bf_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

static BasicBlock* DLJIT_blkfinal_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();

    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long blklen = dl[sp].s.bi_t.blklen;
    void* offsets = (void*)dl[sp].s.bi_t.offsets;
    long oldsize = dl[sp].s.bi_t.oldsize;
    long blksize = blklen * oldsize;

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBlklen = env.int64Const(blklen);
    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmBlksize = env.int64Const(blksize);
    const Constant *llvmOffsets = env.int64Const((long)offsets);
    
    Function *packFn = DLJIT_gen_blkidxfinal(env, offsets, blklen, oldsize, extent, flags, pack);

    auto bbCount = new BasicBlock("bf_pop_count", fn, bbIns);
    auto bbSome = new BasicBlock("bf_pop_some", fn, bbIns);
    auto bbNone = new BasicBlock("bf_pop_none", fn, bbIns);
    auto bbDone = new BasicBlock("bf_pop_done", fn, bbIns);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *stack = env.stack();

    /* Entry */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "bf_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, bf_pops[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *left = builder.getRegister();
    const Register *idx  = builder.getRegister();
    const Register *ct   = builder.getRegister();

    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbDone, bbCount);
    
    /* count */
    builder.SetInsertPoint(bbCount);
    builder.CreateSub(idx, llvmCount, left);
    builder.CreateDiv(ct, sizeleft, llvmBlksize);
    builder.CreateCondBr(builder.CreateICmpGT(ct, env.int64Const(0)),
                          bbSome, bbNone);

    /* if(count > 0) */
    builder.SetInsertPoint(bbSome);
    builder.CreateCondMove(builder.CreateICmpGT(ct, left), ct, left);
    const Register *tmp = builder.getRegister();
    builder.CreateAdd(tmp, idx, ct);
    if(pack) {
        builder.CreateCall(packFn, outbase, base, idx, tmp);
        builder.CreateMove(outbase, builder.ret());
    } else {
        builder.CreateCall(packFn, base, inbase, idx, tmp);
        builder.CreateMove(inbase, builder.ret());
    }
    builder.clearRegister(tmp);
    const Register *copied = builder.getRegister();
    builder.CreateMul(copied, ct, llvmBlksize);
    builder.CreateSub(sizeleft, sizeleft, copied);
    builder.CreateSub(left, left, ct);
    builder.clearRegister(copied);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbDone, bbNone);

    /* None */
    builder.SetInsertPoint(bbNone);
    builder.CreateAdd(idx, idx, ct);
    builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateLoad(offset, offsetsR, 0, idx, 8);
    if(pack) {
        builder.CreatePtr(inbase, base, 0, offset);
    } else {
        builder.CreatePtr(outbase, base, 0, offset);
    }
    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);
    builder.CreateBr(bbInner);

    /* if(stack[sp].countLeft == 0) */
    builder.SetInsertPoint(bbDone);
    if(pack)
        builder.CreateAdd(inbase, base, llvmExtent);
    else 
        builder.CreateAdd(outbase, base, llvmExtent);
    builder.CreateBr(bbOuter);

    builder.clearRegister(ct);
    builder.clearRegister(idx);
    builder.clearRegister(left);
    builder.clearRegister(base);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "bf_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_blockindexedfinal(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();

    BasicBlock *bbPop = new BasicBlock("bf_pop", fn, bbIns);
    BasicBlock *bbInner = DLJIT_contigfinal(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner)
        return nullptr;
    BasicBlock *bbPush = new BasicBlock("bf_push", fn, bbInner);
    auto rvPop = DLJIT_blkfinal_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_blkfinal_push(env, sp, bbPush, bbOuter, bbInner, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}
