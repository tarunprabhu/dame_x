#ifndef TLP_DATATYPES_MYJIT_BASICBLOCK_H
#define TLP_DATATYPES_MYJIT_BASICBLOCK_H

#include "value.h"

#include <set>
#include <string>
#include <vector>

#include <stdint.h>

// Forward declaration to avoid circular references
class Function;
class Instruction;

class BasicBlock {
private:
    std::string _name;
    // This is the address of the block in the emitted code. It'll be used to 
    // in the jump instructions
    bool addrset;
    unsigned long _addr;
    BlockAddress *_blockaddress;
    Function *_parent;
    std::vector<Instruction*> insts;
public:
    BasicBlock(std::string name, Function *fn, BasicBlock *before=nullptr);
    ~BasicBlock();

    std::string name()           const { return _name; }
    Function *parent()           const { return _parent; }
    unsigned long addr()         const { return _addr; }
    bool isaddrset()             const { return addrset; }
    BlockAddress *blockaddress() const { return _blockaddress; }

    void setParent(Function *fn)    { _parent = fn; }

    void append(Instruction* inst);
    void insertBefore(Instruction *n, Instruction *loc);

    bool pass1(unsigned&);
    bool pass2(std::vector<byte>&, unsigned&);
    void gas(std::ostream&, unsigned&, int=0) const;
};


#endif
