#include "jit.h"

#include "util.h"

#include <cstring>
#include <sstream>

using namespace std;

static bool dljit_gen_vecfinal(BuildEnv &env, Function *fn, BasicBlock *bb, BasicBlock *bbExit, BasicBlock *bbIns, const Register *dst, const Register *src, const Register *count, long stride, long blksize, bool pack, bool aligned) {
    Builder &builder = env.builder();

    const Constant *llvmBlksize = env.int64Const(blksize);
    const Constant *llvmStride = nullptr;
    if(stride > 0)
        llvmStride = env.int64Const(stride);
    else
        llvmStride = env.int64Const(-stride);
    BasicBlock *bbOuterT = new BasicBlock("outer_t", fn, bbIns);

#ifdef DEBUG
    builder.startCheck("dljit_gen_vf");
#endif

    builder.SetInsertPoint(bb);
    if(not dljit_gen_memcpy_n(env, fn, bb, bbOuterT, bbOuterT, dst, src, blksize, aligned))
        return false;

    builder.SetInsertPoint(bbOuterT);
    if(pack) {
        builder.CreateAdd(dst, dst, llvmBlksize);
        if(stride > 0)
            builder.CreateAdd(src, src, llvmStride);
        else
            builder.CreateSub(src, src, llvmStride);
    } else {
        if(stride > 0)
            builder.CreateAdd(dst, dst, llvmStride);
        else
            builder.CreateSub(dst, dst, llvmStride);
        builder.CreateAdd(src, src, llvmBlksize);
    }
    builder.CreateDec(count);
    builder.CreateCondBr(BranchType::NZ, bb, bbExit);

#ifdef DEBUG
    builder.endCheck("dljit_gen_vf");
#endif

    return true;
}

Function* DLJIT_gen_vecfinal(BuildEnv &env, long stride, long blklen, long oldsize, long extent, unsigned flags, bool pack) {
    Module& module = env.module();
    Builder &builder = env.builder();

    stringstream ss;
    ss << "DL_vectorfinal_" << (pack ? "pack" : "unpack") << "_"
       << stride << "_" << blklen << "_" << oldsize;
    string fname = ss.str();

    long blksize = blklen*oldsize;

    // This is a terrible variable name but basically tells you whether the
    // type is always aligned if the (un)pack buffer is aligned
    int isaligned = flag_aligned(flags);
    
    Function *fn = module.getFunction(fname);
    if(fn) return fn;

    fn = module.getOrInsertFunction(fname);

    BasicBlock *bbExit = new BasicBlock("exit", fn);
    BasicBlock *bbEntry = new BasicBlock("entry", fn, bbExit);
    BasicBlock *bbUnaligned = new BasicBlock("unaligned", fn, bbExit);
    BasicBlock *bbAligned = nullptr;
    if(isaligned) 
        bbAligned = new BasicBlock("aligned", fn, bbExit);

    builder.SetInsertPoint(bbEntry);
    builder.StartFunction();
#ifdef DEBUG
    builder.startCheck("DLJIT_gen_vf");
#endif
    const Register *dst = builder.getRegister();
    const Register *src = builder.getRegister();
    const Register *ct = builder.getRegister();
    
    builder.CreatePush(builder.bp());
    builder.CreateMove(builder.bp(), builder.sp());
    builder.CreatePtr(builder.sp(), builder.sp(), -24);
    builder.CreateStore(builder.argReg(0), builder.sp(), 0);
    builder.CreateStore(builder.argReg(1), builder.sp(), 8);
    builder.CreateStore(builder.argReg(2), builder.sp(), 16);
    builder.CreateLoad(dst, builder.sp(), 0);
    builder.CreateLoad(src, builder.sp(), 8);
    builder.CreateLoad(ct, builder.sp(), 16);
    const Register *t = builder.getRegister();
    builder.CreateMul(t, ct, env.int64Const(blksize));
    if(pack) {
        builder.CreatePtr(t, dst, 0, t);
    } else {
        builder.CreatePtr(t, src, 0, t);
    }
    builder.CreateStore(t, builder.sp());
    builder.clearRegister(t);

    if(isaligned and blksize <= DLJIT_THRESHOLD_USE_MEMCPY) {
        const Register *tmp = builder.getRegister();
        builder.CreateOr(tmp, dst, src);
        builder.CreateAnd(tmp, tmp, env.int64Const(32-1));
        builder.clearRegister(tmp);
        
        builder.SetInsertPoint(bbAligned);
        if(not dljit_gen_vecfinal(env, fn, bbAligned, bbExit, bbUnaligned, dst, src, ct, stride, blksize, pack, true))
            return nullptr;
    }

    builder.SetInsertPoint(bbUnaligned);
    if(not dljit_gen_vecfinal(env, fn, bbUnaligned, bbExit, bbExit, dst, src, ct, stride, blksize, pack, false))
        return nullptr;
    
    builder.SetInsertPoint(bbExit);
    const Register *rv = builder.getRegister();
    builder.CreateLoad(rv, builder.sp());
    builder.clearRegister(rv);

    builder.CreateLoad(builder.ret(), builder.sp());
    builder.CreatePtr(builder.sp(), builder.sp(), 24);
    builder.CreatePop(builder.bp());
    builder.CreateRet();

    builder.clearRegister(ct);
    builder.clearRegister(src);
    builder.clearRegister(dst);
    
#ifdef DEBUG
    builder.endCheck("DLJIT_gen_vf");
#endif
    builder.EndFunction();
    
    return fn;
}


static bool dljit_gen_blkidxfinal(BuildEnv &env, Function *fn, BasicBlock *bb, BasicBlock *bbExit, BasicBlock *bbIns, const Register *dst, const Register *src, const Register *begin, const Register *end, void *offsets, long blksize, bool pack, bool aligned) {
    Builder &builder = env.builder();

    const Constant *llvmBlksize = env.int64Const(blksize);
    const Constant *llvmOffsets = env.int64Const((long)offsets);

    BasicBlock *bbBody = new BasicBlock("body", fn, bbIns);
    BasicBlock *bbTail = new BasicBlock("tail", fn, bbIns);

#ifdef DEBUG
    builder.startCheck("dljit_gen_bf");
#endif
    const Register *offsetsR = builder.getRegister();
    const Register *base = builder.getRegister();
    builder.SetInsertPoint(bb);
    builder.CreateMove(offsetsR, llvmOffsets);
    if(pack) {
        builder.CreateMove(base, src);
    } else {
        builder.CreateMove(base, dst);
    }
    builder.CreateBr(bbBody);
    
    builder.SetInsertPoint(bbBody);
    const Register *offset = builder.getRegister();
    builder.CreateLoad(offset, offsetsR, 0, begin, 8);

    if(pack) {
        builder.CreatePtr(src, base, 0, offset);
    } else {
        builder.CreatePtr(dst, base, 0, offset);
    }
    builder.clearRegister(offset);

    if(not dljit_gen_memcpy_n(env, fn, bbBody, bbTail, bbTail, dst, src, blksize, aligned))
        return false;

    builder.SetInsertPoint(bbTail);
    if(pack)
        builder.CreateAdd(dst, dst, llvmBlksize);
    else
        builder.CreateAdd(src, src, llvmBlksize);
    builder.CreateInc(begin);
    builder.CreateCondBr(builder.CreateICmpLT(begin, end),
                         bbBody, bbExit);

    builder.clearRegister(base);
    builder.clearRegister(offsetsR);

#ifdef DEBUG
    builder.endCheck("dljit_gen_bf");
#endif
    return true;
}

Function *DLJIT_gen_blkidxfinal(BuildEnv &env, void *offsets, long blklen, long oldsize, long extent, unsigned flags, bool pack) {
    Builder &builder = env.builder();
    Module &module = env.module();    

    stringstream ss;
    ss << "DL_blockindexedfinal_" << (pack ? "pack" : "unpack") << "_"
       << offsets << "_" << blklen << "_" << oldsize;
    string fname = ss.str();

    long blksize = blklen*oldsize;

    // This is a terrible variable name but basically tells you whether the
    // type is always aligned if the (un)pack buffer is aligned
    int isaligned = flag_aligned(flags);
    
    Function *fn = module.getFunction(fname);
    if(fn) return fn;

    fn = module.getOrInsertFunction(fname);

    BasicBlock *bbExit = new BasicBlock("exit", fn);
    BasicBlock *bbEntry = new BasicBlock("entry", fn, bbExit);
    BasicBlock *bbUnaligned = new BasicBlock("unaligned", fn, bbExit);
    BasicBlock *bbAligned = nullptr;
    if(isaligned) 
        bbAligned = new BasicBlock("aligned", fn, bbExit);

    builder.SetInsertPoint(bbEntry);
    builder.StartFunction();
#ifdef DEBUG
    builder.startCheck("DLJIT_gen_bf");
#endif
    const Register *dst = builder.getRegister();
    const Register *src = builder.getRegister();
    const Register *begin = builder.getRegister();
    const Register *end = builder.getRegister();

    builder.CreatePush(builder.bp());
    builder.CreateMove(builder.bp(), builder.sp());
    builder.CreatePtr(builder.sp(), builder.sp(), -32);
    builder.CreateStore(builder.argReg(0), builder.sp(), 0);
    builder.CreateStore(builder.argReg(1), builder.sp(), 8);
    builder.CreateStore(builder.argReg(2), builder.sp(), 16);
    builder.CreateStore(builder.argReg(3), builder.sp(), 24);
    builder.CreateLoad(dst, builder.sp(), 0);
    builder.CreateLoad(src, builder.sp(), 8);
    builder.CreateLoad(begin, builder.sp(), 16);
    builder.CreateLoad(end, builder.sp(), 24);

    const Register *t = builder.getRegister();
    builder.CreateSub(t, end, begin);
    builder.CreateMul(t, t, env.int64Const(blksize));
    if(pack) {
        builder.CreatePtr(t, dst, 0, t);
    } else {
        builder.CreatePtr(t, src, 0, t);
    }
    builder.CreateStore(t, builder.sp());
    builder.clearRegister(t);

    if(isaligned and blksize <= DLJIT_THRESHOLD_USE_MEMCPY) {
        const Register *tmp = builder.getRegister();
        builder.CreateOr(tmp, dst, src);
        builder.CreateAnd(tmp, tmp, env.int64Const(32-1));
        builder.clearRegister(tmp);
        builder.CreateCondBr(BranchType::Z, bbAligned, bbUnaligned);
        
        builder.SetInsertPoint(bbAligned);
        if(not dljit_gen_blkidxfinal(env, fn, bbAligned, bbExit, bbUnaligned, dst, src, begin, end, offsets, blksize, pack, true))
            return nullptr;
    }

    builder.SetInsertPoint(bbUnaligned);
    if(not dljit_gen_blkidxfinal(env, fn, bbUnaligned, bbExit, bbExit, dst, src, begin, end, offsets, blksize, pack, false))
        return nullptr;
    
    builder.SetInsertPoint(bbExit);
    
    builder.CreateLoad(builder.ret(), builder.sp());
    builder.CreatePtr(builder.sp(), builder.sp(), 32);
    builder.CreatePop(builder.bp());
    builder.CreateRet();

    builder.clearRegister(end);
    builder.clearRegister(begin);
    builder.clearRegister(src);
    builder.clearRegister(dst);
    
#ifdef DEBUG
    builder.endCheck("DLJIT_gen_bf");
#endif
    builder.EndFunction();
    
    return fn;
}


static bool dljit_gen_idxfinal(BuildEnv &env, Function *fn, BasicBlock *bb, BasicBlock *bbExit, BasicBlock *bbIns, const Register *dst, const Register *src, const Register *begin, const Register *end, const Register *copied, void *offsets, void* blklens, long oldsize, bool pack, bool aligned) {
    Builder &builder = env.builder();
    Module &module = env.module();

    const Constant *llvmOldsize = env.int64Const((long)oldsize);
    const Constant *llvmOffsets = env.int64Const((long)offsets);
    const Constant *llvmBlklens = env.int64Const((long)blklens);

    auto memcpyFn = module.getLibraryFunction("memcpy", (void*)memcpy);
    
    BasicBlock *bbBody = new BasicBlock("body", fn, bbIns);
    BasicBlock *bbTail = new BasicBlock("tail", fn, bbIns);

    builder.SetInsertPoint(bb);
#ifdef DEBUG
    builder.startCheck("dljit_gen_if");
#endif
    const Register *offsetsR = builder.getRegister();
    const Register *blklensR = builder.getRegister();
    const Register *base = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateMove(blklensR, llvmBlklens);
    if(pack) {
        builder.CreateMove(base, src);
    } else {
        builder.CreateMove(base, dst);
    }
    builder.CreateBr(bbBody);
    
    builder.SetInsertPoint(bbBody);
    const Register *offset = builder.getRegister();
    builder.CreateLoad(offset, offsetsR, 0, begin, 8);
    if(pack) {
        builder.CreatePtr(src, base, 0, offset);
    } else {
        builder.CreatePtr(dst, base, 0, offset);
    }
    builder.clearRegister(offset);

    const Register *blksize = builder.getRegister();
    builder.CreateLoad(blksize, blklensR, 0, begin, 8);
    builder.CreateMul(blksize, blksize, llvmOldsize);
    builder.CreateAdd(copied, copied, blksize);

    // TODO: Don't always use memcpy
    builder.CreateCall(memcpyFn, dst, src, blksize);
    
    builder.SetInsertPoint(bbTail);
    if(pack)
        builder.CreateAdd(dst, dst, blksize);
    else
        builder.CreateAdd(src, src, blksize);
    builder.CreateInc(begin);
    builder.CreateCondBr(builder.CreateICmpLT(begin, end),
                         bbBody, bbExit);

    builder.clearRegister(blksize);
    builder.clearRegister(base);
    builder.clearRegister(blklensR);
    builder.clearRegister(offsetsR);

#ifdef DEBUG
    builder.endCheck("dljit_gen_if");
#endif
    return true;
}


Function *DLJIT_gen_idxfinal(BuildEnv &env, void *offsets, void *blklens, long oldsize, long extent, unsigned flags, bool pack) {
    Builder &builder = env.builder();
    Module &module = env.module();    

    stringstream ss;
    ss << "DL_indexedfinal_" << (pack ? "pack" : "unpack") << "_"
       << offsets << "_" << blklens << "_" << oldsize;
    string fname = ss.str();

    // This is a terrible variable name but basically tells you whether the
    // type is always aligned if the (un)pack buffer is aligned
    int isaligned = flag_aligned(flags);
    
    Function *fn = module.getFunction(fname);
    if(fn) return fn;

    fn = module.getOrInsertFunction(fname);

    BasicBlock *bbExit = new BasicBlock("exit", fn);
    BasicBlock *bbEntry = new BasicBlock("entry", fn, bbExit);
    BasicBlock *bbUnaligned = new BasicBlock("unaligned", fn, bbExit);
    BasicBlock *bbAligned = nullptr;
    if(isaligned) 
        bbAligned = new BasicBlock("aligned", fn, bbExit);

    builder.SetInsertPoint(bbEntry);
    builder.StartFunction();
#ifdef DEBUG
    builder.startCheck("DLJIT_gen_if");
#endif
    // We don't need all of them, but we're going to save all the callee-saved
    // registers because it's easier this way
    DLJIT_gen_function_header(env, bbEntry);
    
    builder.CreatePtr(builder.sp(), builder.sp(), -32);
    builder.CreateStore(builder.argReg(0), builder.sp(), 0);
    builder.CreateStore(builder.argReg(1), builder.sp(), 8);
    builder.CreateStore(builder.argReg(2), builder.sp(), 16);
    builder.CreateStore(builder.argReg(3), builder.sp(), 24);

    // We're doing this manually and we're not going to reuse these registers
    // but I have to be very careful about this
    const Register *dst = builder.getRegister(1);
    const Register *src = builder.getRegister(1);
    const Register *begin = builder.getRegister(1);
    const Register *end = builder.getRegister(1);
    builder.CreateLoad(dst, builder.sp(), 0);
    builder.CreateLoad(src, builder.sp(), 8);
    builder.CreateLoad(begin, builder.sp(), 16);
    builder.CreateLoad(end, builder.sp(), 24);

    const Register *copied = builder.getRegister();
    builder.CreateMove(copied, env.int64Const(0));

    if(isaligned) {
        const Register *tmp = builder.getRegister();
        builder.CreateOr(tmp, dst, src);
        builder.CreateAnd(tmp, tmp, env.int64Const(32-1));
        builder.clearRegister(tmp);
        builder.CreateCondBr(BranchType::Z, bbAligned, bbUnaligned);
        
        builder.SetInsertPoint(bbAligned);
        if(not dljit_gen_idxfinal(env, fn, bbAligned, bbExit, bbUnaligned, dst, src, begin, end, copied, offsets, blklens, oldsize, pack, true))
            return nullptr;
    }

    builder.SetInsertPoint(bbUnaligned);
    if(not dljit_gen_idxfinal(env, fn, bbUnaligned, bbExit, bbExit, dst, src, begin, end, copied, offsets, blklens, oldsize, pack, false))
        return nullptr;
    
    builder.SetInsertPoint(bbExit);
    const Register *t = builder.getRegister();
    if(pack) {
        builder.CreateLoad(t, builder.sp());
    } else {
        builder.CreateLoad(t, builder.sp(), 8);
    }
    builder.CreatePtr(builder.ret(), t, 0, copied);
    builder.clearRegister(t);
    builder.CreatePtr(builder.sp(), builder.sp(), 32);
    DLJIT_gen_function_footer(env, bbExit);

    builder.clearRegister(copied);
    
#ifdef DEBUG
    builder.endCheck("DLJIT_gen_if");
#endif
    builder.EndFunction();
    
    return fn;
}
