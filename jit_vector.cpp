#include "jit.h"

#include <iostream>
#include <sstream>

using namespace std;

#ifdef DEBUG
const char *vec_pushes[] = { "vec0_push", "vec1_push", "vec2_push",
                             "vec3_push", "vec4_push", "vec5_push",
                             "vec6_push", "vec7_push", "vec8_push",
                             "vec9_push", "vec10_push", "vec11_push",
                             "vec12_push", "vec13_push", "vec14_push" };
const char *vec_pops[] = { "vec0_pop", "vec1_pop", "vec2_pop",
                           "vec3_pop", "vec4_pop", "vec5_pop",
                           "vec6_pop", "vec7_pop", "vec8_pop",
                           "vec9_pop", "vec10_pop", "vec11_pop",
                           "vec12_pop", "vec13_pop", "vec14_pop" };


const char *vf_pushes[] = { "vf0_push", "vf1_push", "vf2_push",
                             "vf3_push", "vf4_push", "vf5_push",
                             "vf6_push", "vf7_push", "vf8_push",
                             "vf9_push", "vf10_push", "vf11_push",
                             "vf12_push", "vf13_push", "vf14_push" };
const char *vf_pops[] = { "vf0_pop", "vf1_pop", "vf2_pop",
                           "vf3_pop", "vf4_pop", "vf5_pop",
                           "vf6_pop", "vf7_pop", "vf8_pop",
                           "vf9_pop", "vf10_pop", "vf11_pop",
                           "vf12_pop", "vf13_pop", "vf14_pop" };
#endif


static BasicBlock* DLJIT_vec_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBlklen = env.int64Const(dl[sp].s.v_t.blklen);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
    
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vec_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, vec_pushes[sp]);
    }
#endif
    if(pack)
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
    else
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
    // stack[sp].countLeft = dl[sp].count
    builder.CreateStore(llvmCount, stack, disp_stack(sp, countLeft));
                        
    if(dl[sp].kind != DL_VECTOR1) {
        if(pack)
            builder.CreateStore(inbase, stack, disp_stack(sp+1, base));
        else
            builder.CreateStore(outbase, stack, disp_stack(sp+1, base));
        // stack[sp+1].countLeft = dl[sp].s.v_t.blklen
        builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    }
    builder.CreateBr(bbInner);
    
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vec_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

static BasicBlock *DLJIT_vec_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();
    Function *fn = env.fn();
    stringstream ss;

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBlklen = env.int64Const(dl[sp].s.v_t.blklen);
    const Constant *llvmStride = env.int64Const(dl[sp].s.v_t.stride, true);
    
    ss.str(string());
    ss << "vec" << sp << "_notdone";
    BasicBlock *bbNotDone = new BasicBlock(ss.str(), fn, bbIns);
    
    /* Check if the type is done */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vec_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, vec_pops[sp]);
    }
#endif
    const Register *left = builder.getRegister();
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbOuter, bbNotDone);
    
    /* Not done with the type */
    builder.SetInsertPoint(bbNotDone);
    const Register *base = builder.getRegister();
    const Register *idx = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateSub(idx, llvmCount, left);
    builder.CreateMul(offset, idx, llvmStride);
    if(pack) {
        builder.CreatePtr(inbase, base, 0, offset);
    } else {
        builder.CreatePtr(outbase, base, 0, offset);
    }
    builder.clearRegister(offset);
    builder.clearRegister(idx);
    builder.clearRegister(base);
    builder.clearRegister(left);
        
    if(dl[sp].kind != DL_VECTOR1)
        // stack[sp+1].countLeft = dl[sp].s.v_t.blklen*dl[sp].s.v_t.oldsize
        builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    
    builder.CreateBr(bbInner);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "vec_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_vector(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();
    stringstream ss;

    ss.str(string());
    ss << "vec" << sp << "_pop";
    BasicBlock *bbPop = new BasicBlock(ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(string());
    ss << "vec" << sp << "_push";
    BasicBlock *bbPush = new BasicBlock(ss.str(), fn, bbInner);

    auto rvPop = DLJIT_vec_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_vec_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

/* NOTE: This function returns the last basic block before which it has to 
 * jump to the contigfinal block. This is different from most of the other 
 * functions which return the entry block of the corresponding instruction. 
 * The jump to contigfinal will be inserted after the contigfinal block has been
 * generated (which will be done once pop has been codegen-ed */
static BasicBlock* DLJIT_vecfinal_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();
    Function *fn = env.fn();

    long size = dl[sp].size;
    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long stride = dl[sp].s.v_t.stride;
    long blklen = dl[sp].s.v_t.blklen;
    long oldsize = dl[sp].s.v_t.oldsize;

    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmSize = env.int64Const(size);
    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmStride = env.int64Const(stride, true);
    const Constant *llvmBlklen = env.int64Const(blklen);
    const Constant *llvmBlksize = env.int64Const(blklen*oldsize);

    BasicBlock *bbCountCheck = new BasicBlock("vf_push_ck_count", fn, bbIns);
    BasicBlock *bbAll  = new BasicBlock("vf_push_all", fn, bbIns);
    BasicBlock *bbSome = new BasicBlock("vf_push_some", fn, bbIns);
    BasicBlock *bbNone = new BasicBlock("vf_push_none", fn, bbIns);

    Function *packFn = DLJIT_gen_vecfinal(env, stride, blklen, oldsize, extent, flags, pack);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *stack = env.stack();

    /* Check how many elements can be copied */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vf_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, vf_pushes[sp]);
    }
#endif
    const Register *left = builder.getRegister();
    const Register *base = builder.getRegister();
    builder.CreateMove(left, llvmCount);
    if(pack) {
        builder.CreateMove(base, inbase);
    } else {
        builder.CreateMove(base, outbase);
    }
    builder.CreateStore(base, stack, disp_stack(sp, base));    
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(builder.CreateICmpGE(sizeleft, llvmSize),
                         bbAll, bbCountCheck);
    
    /* Enough space for the full vector */
    builder.SetInsertPoint(bbAll);
    builder.CreateCall(packFn, outbase, inbase, llvmCount);
    if(pack) {
        builder.CreateMove(outbase, builder.ret());
        builder.CreateAdd(inbase, base, llvmExtent);
    } else {
        builder.CreateMove(inbase, builder.ret());        
        builder.CreateAdd(outbase, base, llvmExtent);
    }
    builder.CreateStore(env.int64Const(0), stack, disp_stack(sp, countLeft));
    builder.CreateSub(sizeleft, sizeleft, llvmSize);
    builder.CreateBr(bbOuter);
    
    /* Check how many elements can be copied */
    builder.SetInsertPoint(bbCountCheck);
    const Register *ct = builder.getRegister();
    builder.CreateDiv(ct, sizeleft, llvmBlksize);
    builder.CreateCondBr(builder.CreateICmpGT(ct, env.int32Const(0)),
                         bbSome, bbNone);

    /* Some elements can be copied */
    builder.SetInsertPoint(bbSome);
    builder.CreateCall(packFn, outbase, inbase, ct);
    if(pack) {
        builder.CreateMove(outbase, builder.ret());
    } else {
        builder.CreateMove(inbase, builder.ret());
    }
    const Register *copied = builder.getRegister();
    builder.CreateMul(copied, ct, llvmBlksize);
    builder.CreateSub(sizeleft, sizeleft, copied);
    builder.CreateSub(left, left, ct);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.clearRegister(copied);
    builder.CreateBr(bbNone);

    // Done copying
    builder.SetInsertPoint(bbNone);
    const Register *idx = ct;            // idx = count
                                         // stack[sp+1].countLeft = v_t.blklen
    builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    const Register *offset = idx;        // idx*dl[sp].s.v_t.blklen
    builder.CreateMul(offset, idx, llvmStride);
    if(pack) {
        builder.CreatePtr(inbase, base, 0, offset); 
    } else {
        builder.CreatePtr(outbase, base, 0, offset);
    }
    builder.CreateBr(bbInner);

    builder.clearRegister(ct);
    builder.clearRegister(base);
    builder.clearRegister(left);
    
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vf_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    
    return bb;
}

static BasicBlock* DLJIT_vecfinal_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();
    Function *fn = env.fn();

    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    long stride = dl[sp].s.v_t.stride;
    long blklen = dl[sp].s.v_t.blklen;
    long oldsize = dl[sp].s.v_t.oldsize;

    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmStride = env.int64Const(stride, true);
    const Constant *llvmBlklen = env.int64Const(blklen);
    const Constant *llvmBlksize = env.int64Const(blklen*oldsize);

    Function *packFn = DLJIT_gen_vecfinal(env, stride, blklen, oldsize, extent, flags, pack);
    
    auto bbCountCheck = new BasicBlock("vf_pop_ck_count", fn, bbIns);
    auto bbSome = new BasicBlock("vf_pop_some", fn, bbIns);
    auto bbNone = new BasicBlock("vf_pop_none", fn, bbIns);
    auto bbDone = new BasicBlock("vf_done", fn, bbIns);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *stack = env.stack();

    /* Entry block for pop */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vf_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, vf_pops[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *left = builder.getRegister();
    const Register *ct = builder.getRegister();
    const Register *idx = builder.getRegister();
    
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbDone, bbCountCheck);

    builder.SetInsertPoint(bbDone);
    if(pack) {
        builder.CreateAdd(inbase, base, llvmExtent);
    } else {
        builder.CreateAdd(outbase, base, llvmExtent);
    }
    builder.CreateBr(bbOuter);

    builder.SetInsertPoint(bbCountCheck);
    builder.CreateSub(idx, llvmCount, left);
    builder.CreateDiv(ct, sizeleft, llvmBlksize);
    builder.CreateCondBr(builder.CreateICmpGT(ct, env.int32Const(0)),
                         bbSome, bbNone);

    builder.SetInsertPoint(bbSome);
    const Register *offset = builder.getRegister();
    builder.CreateCondMove(builder.CreateICmpGT(ct, left), ct, left);
    const Register *ptr = builder.getRegister();
    builder.CreateMul(offset, idx, llvmStride);
    builder.CreatePtr(ptr, base, 0, offset);
    if(pack) {
        builder.CreateCall(packFn, outbase, ptr, ct);
        builder.CreateMove(outbase, builder.ret());
    } else {
        builder.CreateCall(packFn, ptr, inbase, ct);
        builder.CreateMove(inbase, builder.ret());
    }
    builder.clearRegister(ptr);
    builder.CreateMul(offset, ct, llvmBlksize);
    builder.CreateSub(sizeleft, sizeleft, offset);

    const Register *newleft = builder.getRegister();
    builder.CreateSub(newleft, left, ct);
    builder.CreateStore(newleft, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbDone, bbNone);
    builder.clearRegister(newleft);

    builder.SetInsertPoint(bbNone);
    const Register *offset1 = builder.getRegister();
    builder.CreateAdd(idx, idx, ct);
    builder.CreateMul(offset1, idx, llvmStride);
    builder.CreateStore(llvmBlklen, stack, disp_stack(sp+1, countLeft));
    if(pack) {
        builder.CreatePtr(inbase, base, 0, offset1);
    } else {
        builder.CreatePtr(outbase, base, 0, offset1);
    }
    builder.clearRegister(offset1);
    builder.CreateBr(bbInner);

    builder.clearRegister(offset);
    builder.clearRegister(idx);
    builder.clearRegister(ct);
    builder.clearRegister(left);
    builder.clearRegister(base);
    
#ifdef DEBUG
    {
        stringstream ss;
        ss << "vf_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_vectorfinal(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();

    BasicBlock *bbPop = new BasicBlock("vf_pop", fn, bbIns);
    BasicBlock *bbInner = DLJIT_contigfinal(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner)
        return nullptr;
    BasicBlock *bbPush = new BasicBlock("vf_push", fn, bbInner);
    auto rvPop = DLJIT_vecfinal_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_vecfinal_push(env, sp, bbPush, bbOuter, bbInner, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}
