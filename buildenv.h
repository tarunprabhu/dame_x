#ifndef TLP_DATATYPES_MYJIT_BUILDENV_H
#define TLP_DATATYPES_MYJIT_BUILDENV_H

#include "mpi.h"
#include "dataloop.h"
#include "builder.h"
#include "function.h"
#include "module.h"
#include "instruction.h"
#include "value.h"

#include <map>
#include <vector>

#include <stdint.h>

class BuildEnv {
private:
    const Dataloop *_dl;
    Builder _builder;
    Module &_module;

    Function *_function;

    const Register *_inbase, *_outbase;
    const Register *_sizeleft;
    const Register *_stack;
    const Register *_partial;

    int _paramState;
    int _paramSize;
    int _paramCopied;
    int _retval;
    int _jtaddr;
    int _resumeAddr;

    BasicBlock *_bbTail;

    std::map<uint64_t, const Constant*> consts64;
    std::map<uint32_t, const Constant*> consts32;
    std::map<uint8_t,  const Constant*> consts8;

public:
    BuildEnv(const Dataloop*, Module&, Function*);
    ~BuildEnv();

    const Dataloop *dl()       const { return _dl; }
    Builder& builder()               { return _builder; }
    Function* fn()                   { return _function; }
    Module& module()                 { return _module; }

    const Register* inbase()   const { return _inbase; }
    const Register* outbase()  const { return _outbase; }
    const Register* sizeleft() const { return _sizeleft; }
    const Register* partial()  const { return _partial; }
    const Register* stack()    const { return _stack; }

    void setInbase(const Register *r)   { _inbase = r; }
    void setOutbase(const Register *r)  { _outbase = r; }
    void setSizeleft(const Register *r) { _sizeleft = r; }
    void setStack(const Register *r)    { _stack = r; }
    void setPartial(const Register *r)  { _partial = r; }
    
    BasicBlock* bbTail()       const { return _bbTail; }
    
    const Constant* int64Const(int64_t val, bool sign=false);
    const Constant* int32Const(int32_t val, bool sign=false);
    const Constant* int8Const(int8_t val, bool sign=false);

    int paramState()  const { return _paramState; }
    int paramSize()   const { return _paramSize; }
    int paramCopied() const { return _paramCopied; }
    int retval()      const { return _retval; }
    int jtaddr()      const { return _jtaddr; }
    int resumeAddr()  const { return _resumeAddr; }

    void setBBTail(BasicBlock *bbTail) { _bbTail = bbTail; }
    void setDataloop(const Dataloop *dl) { _dl = dl; }
};

#endif
