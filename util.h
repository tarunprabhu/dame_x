#ifndef TLP_DATATYPES_MYJIT_UTIL_H
#define TLP_DATATYPES_MYJIT_UTIL_H

#include <stdint.h>

#include <string>

#include "jit.h"

void* DLJIT_malloc_exec(size_t sz);

unsigned dljit_bits(int64_t val, unsigned min=32);
unsigned dljit_bytes(int64_t val, unsigned min=4);
void dljit_error(std::string str);

bool DLJIT_gen_function_header(BuildEnv &env, BasicBlock *bb);
bool DLJIT_gen_function_footer(BuildEnv &env, BasicBlock *bb);

// This function generates a memcpy function which chooses aligned or
// unaligned versions as appropriate
Function* DLJIT_gen_memcpy_n(BuildEnv &env, unsigned long);

// This generates an inlined memcpy with the alignment specified. This is used
// by DLJIT_gen_memcpy_n and others
bool dljit_gen_memcpy_n(BuildEnv &env, Function *fn, BasicBlock *bb, BasicBlock *bbExit, BasicBlock *bbIns, const Register *dst, const Register *src, unsigned long tocopy, bool aligned);

#endif
