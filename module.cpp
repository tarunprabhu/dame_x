#include "module.h"

#include "basicblock.h"

#include <iostream>

using namespace std;

Module::~Module()  {
    for(auto f : functions)
        delete f.second;
}

Function* Module::getFunction(string name) const {
    auto it = functions.find(name);
    if(it != functions.end())
        return it->second;
    return nullptr;
}

LibraryFunction* Module::getLibraryFunction(string name, void *ptr) {
    LibraryFunction *fn = static_cast<LibraryFunction*>(getFunction(name));
    if(fn)
        return fn;
    fn = new LibraryFunction(name, ptr);
    functions[name] = fn;
    return fn;
}

Function* Module::getOrInsertFunction(string name) {
    Function *fn = getFunction(name);
    if(fn)
        return fn;
    functions[name] = new Function(name);
    return functions.at(name);
}

void Module::gas(ostream &os, int opt) const {
    unsigned pos = 0;
    
    os << "/* -----  module  ---- */" << endl;
    Function *fn = getFunction("DL_pack");
    pos = fn->jtcurr()*8;
    for(auto f : fn->buildorder())
        f->gas(os, pos, opt);

    pos = fn->jtcurr()*8;
    for(auto f : getFunction("DL_unpack")->buildorder())
        f->gas(os, pos, opt);
    
    os << "/* -----  ------  -----*/" << endl;
}
