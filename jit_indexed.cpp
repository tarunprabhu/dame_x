#include "jit.h"

#include <sstream>

using namespace std;


#ifdef DEBUG
const char *idx_pushes[] = { "idx0_push", "idx1_push", "idx2_push",
                             "idx3_push", "idx4_push", "idx5_push",
                             "idx6_push", "idx7_push", "idx8_push",
                             "idx9_push", "idx10_push", "idx11_push",
                             "idx12_push", "idx13_push", "idx14_push" };
const char *idx_pops[] = { "idx0_pop", "idx1_pop", "idx2_pop",
                           "idx3_pop", "idx4_pop", "idx5_pop",
                           "idx6_pop", "idx7_pop", "idx8_pop",
                           "idx9_pop", "idx10_pop", "idx11_pop",
                           "idx12_pop", "idx13_pop", "idx14_pop" };

const char *if_pushes[] = { "if0_push", "if1_push", "if2_push",
                             "if3_push", "if4_push", "if5_push",
                             "if6_push", "if7_push", "if8_push",
                             "if9_push", "if10_push", "if11_push",
                             "if12_push", "if13_push", "if14_push" };
const char *if_pops[] = { "if0_pop", "if1_pop", "if2_pop",
                           "if3_pop", "if4_pop", "if5_pop",
                           "if6_pop", "if7_pop", "if8_pop",
                           "if9_pop", "if10_pop", "if11_pop",
                           "if12_pop", "if13_pop", "if14_pop" };
#endif


static BasicBlock* DLJIT_idx_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBlklen0 = env.int64Const(dl[sp].s.i_t.blklens[0]);
    const Constant *llvmOffset0 = env.int64Const(dl[sp].s.i_t.offsets[0]);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
    
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "idx_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, idx_pushes[sp]);
    }
#endif
    if(pack) {
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
        builder.CreateStore(inbase, stack, disp_stack(sp+1, base));
        builder.CreateAdd(inbase, inbase, llvmOffset0);
    } else {
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
        builder.CreateStore(outbase, stack, disp_stack(sp+1, base));
        builder.CreateAdd(outbase, outbase, llvmOffset0);
    }

    builder.CreateStore(llvmCount, stack, disp_stack(sp, countLeft));
    builder.CreateStore(llvmBlklen0, stack, disp_stack(sp+1, countLeft));
    
    // goto dlpush
    builder.CreateBr(bbInner);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "idx_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif

    return bb;
}

static BasicBlock* DLJIT_idx_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    stringstream ss;

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmOffsets = env.int64Const((long)dl[sp].s.i_t.offsets);
    const Constant *llvmBlklens = env.int64Const((long)dl[sp].s.i_t.blklens);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
    
    ss.str(string());
    ss << "idx" << sp << "_notdone";
    BasicBlock *bbNotDone = new BasicBlock(ss.str(), fn, bbIns);

    // if(stack[sp].countLeft == 0)
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "idx_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, idx_pops[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *left = builder.getRegister();
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbOuter, bbNotDone);

    builder.SetInsertPoint(bbNotDone);
    const Register *idx = builder.getRegister();
    builder.CreateSub(idx, llvmCount, left);

    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateLoad(offset, offsetsR, 0, idx, 8);
    if(pack)
        builder.CreatePtr(inbase, base, 0, offset);
    else
        builder.CreatePtr(outbase, base, 0, offset);
    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);
    
    const Register *blklensR = builder.getRegister();
    const Register *blklen = builder.getRegister();
    builder.CreateMove(blklensR, llvmBlklens);
    builder.CreateLoad(blklen, blklensR, 0, idx, 8);
    builder.CreateStore(blklen, stack, disp_stack(sp+1, countLeft));
    builder.clearRegister(blklen);
    builder.clearRegister(blklensR);

    builder.CreateBr(bbInner);

    builder.clearRegister(idx);
    builder.clearRegister(left);
    builder.clearRegister(base);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "idx_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_indexed(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();
    stringstream ss;

    ss.str(string());
    ss << "idx" << sp << "_pop";
    BasicBlock *bbPop = new BasicBlock(ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(string());
    ss << "idx" << sp << "_push";
    BasicBlock *bbPush = new BasicBlock(ss.str(), fn, bbInner);

    auto rvPop = DLJIT_idx_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_idx_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

static BasicBlock* DLJIT_idxfinal_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    
    long size = dl[sp].size;
    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    void* blklens = (void*)dl[sp].s.i_t.blklens;
    void* offsets = (void*)dl[sp].s.i_t.offsets;
    long oldsize = dl[sp].s.i_t.oldsize;

    const Constant *llvmSize = env.int64Const(size);
    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmOffsets = env.int64Const((long)offsets);
    const Constant *llvmBlklens = env.int64Const((long)blklens);
    const Constant *llvmOldsize = env.int64Const(oldsize);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *stack = env.stack();
    
    Function *packFn = DLJIT_gen_idxfinal(env, offsets, blklens, oldsize, extent, flags, pack);

    BasicBlock *bbAll = new BasicBlock("if_push_all", fn, bbIns);
    BasicBlock *bbCountHead = new BasicBlock("if_push_count_head", fn, bbIns);
    BasicBlock *bbCountBody = new BasicBlock("if_push_count_body", fn, bbIns);
    BasicBlock *bbCountTail = new BasicBlock("if_push_count_tail", fn, bbIns);
    BasicBlock *bbCount = new BasicBlock("if_push_count", fn, bbIns);
    BasicBlock *bbSome = new BasicBlock("if_push_some", fn, bbIns);
    BasicBlock *bbNone = new BasicBlock("if_push_none", fn, bbIns);

    /* Entry */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "if_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, if_pushes[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *ct = builder.getRegister();
    const Register *blklensR = builder.getRegister();   
    const Register *left = builder.getRegister();
    builder.CreateStore(llvmCount, stack, disp_stack(sp, countLeft));
    if(pack)
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
    else
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateMove(left, llvmCount);
    builder.CreateCondBr(builder.CreateICmpGE(sizeleft, llvmSize),
                         bbAll, bbCountHead);


    builder.SetInsertPoint(bbAll);
    if(pack) {
        builder.CreateCall(packFn, outbase, base, env.int64Const(0), llvmCount);
        builder.CreateMove(outbase, builder.ret());
        builder.CreateAdd(inbase, base, llvmExtent);
    } else {
        builder.CreateCall(packFn, base, inbase, env.int64Const(0), llvmCount);
        builder.CreateMove(inbase, builder.ret());
        builder.CreateAdd(outbase, base, llvmExtent);
    }
    builder.CreateStore(env.int64Const(0), stack, disp_stack(sp, countLeft));
    builder.CreateSub(sizeleft, sizeleft, llvmSize);
    builder.CreateBr(bbOuter);

    builder.SetInsertPoint(bbCountHead);
    const Register *copied = builder.getRegister();
    builder.CreateMove(ct, env.int64Const(0));
    builder.CreateMove(copied, env.int64Const(0));
    builder.CreateMove(blklensR, llvmBlklens);
    builder.CreateBr(bbCountBody);

    builder.SetInsertPoint(bbCountBody);
    const Register *blksize = builder.getRegister();
    builder.CreateLoad(blksize, blklensR, 0, ct, 8);
    builder.CreateMul(blksize, blksize, llvmOldsize);
    const Register *tmp = builder.getRegister();
    builder.CreateAdd(tmp, copied, blksize);
    builder.CreateCondBr(builder.CreateICmpGT(tmp, sizeleft),
                         bbCount, bbCountTail);
    builder.clearRegister(tmp);

    builder.SetInsertPoint(bbCountTail);
    builder.CreateAdd(copied, copied, blksize);
    builder.clearRegister(blksize);
    builder.CreateInc(ct);
    builder.CreateCondBr(builder.CreateICmpLT(ct, llvmCount),
                         bbCountBody, bbCount);
    

    builder.SetInsertPoint(bbCount);
    builder.CreateCondBr(builder.CreateICmpGT(ct, env.int64Const(0)),
                         bbSome, bbNone);

    builder.SetInsertPoint(bbSome);
    if(pack) {
        builder.CreateCall(packFn, outbase, base, env.int64Const(0), ct);
        builder.CreateMove(outbase, builder.ret());
    } else {
        builder.CreateCall(packFn, base, inbase, env.int64Const(0), ct);
        builder.CreateMove(inbase, builder.ret());
    }
    builder.CreateSub(sizeleft, sizeleft, copied);
    builder.clearRegister(copied);
    builder.CreateSub(left, left, ct);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.clearRegister(left);
    
    builder.SetInsertPoint(bbNone);
    const Register *blklen = builder.getRegister();
    builder.CreateLoad(blklen, blklensR, 0, ct, 8);
    builder.CreateStore(blklen, stack, disp_stack(sp+1, countLeft));
    builder.clearRegister(blklen);
    builder.clearRegister(blklensR);
    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateLoad(offset, offsetsR, 0, ct, 8);
    if(pack)
        builder.CreateAdd(inbase, base, offset);
    else
        builder.CreateAdd(outbase, base, offset);
    builder.CreateBr(bbInner);
    
    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);
    builder.clearRegister(ct);
    builder.clearRegister(base);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "if_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    
    return bb;
}

static BasicBlock* DLJIT_idxfinal_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();

    long count = dl[sp].count;
    long extent = dl[sp].extent;
    unsigned flags = dl[sp].flags;

    void *blklens = (void*)dl[sp].s.i_t.blklens;
    void *offsets = (void*)dl[sp].s.i_t.offsets;
    long oldsize = dl[sp].s.i_t.oldsize;

    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmOldsize = env.int64Const(oldsize);
    const Constant *llvmOffsets = env.int64Const((long)offsets);
    const Constant *llvmBlklens = env.int64Const((long)blklens);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *stack = env.stack();
    
    Function *packFn = DLJIT_gen_idxfinal(env, offsets, blklens, oldsize, extent, flags, pack);

    BasicBlock *bbCountHead = new BasicBlock("if_pop_count_head", fn, bbIns);
    BasicBlock *bbCountBody = new BasicBlock("if_pop_count_body", fn, bbIns);
    BasicBlock *bbCountTail = new BasicBlock("if_pop_count_tail", fn, bbIns);
    BasicBlock *bbCount = new BasicBlock("if_pop_count", fn, bbIns);
    BasicBlock *bbSome = new BasicBlock("if_pop_some", fn, bbIns);
    BasicBlock *bbNone = new BasicBlock("if_pop_none", fn, bbIns);
    BasicBlock *bbDone = new BasicBlock("if_pop_done", fn, bbIns);

    /* Entry */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "if_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, if_pops[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *ct = builder.getRegister();
    const Register *blklensR = builder.getRegister();
    const Register *left = builder.getRegister();
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbDone, bbCountHead);

    builder.SetInsertPoint(bbDone);
    if(pack)
        builder.CreateAdd(inbase, base, llvmExtent);
    else
        builder.CreateAdd(outbase, base, llvmExtent);
    builder.CreateBr(bbOuter);

    builder.SetInsertPoint(bbCountHead);
    const Register *copied = builder.getRegister();
    builder.CreateSub(ct, llvmCount, left);
    builder.CreateMove(copied, env.int64Const(0));
    builder.CreateMove(blklensR, llvmBlklens);
    builder.CreateBr(bbCountBody);

    builder.SetInsertPoint(bbCountBody);
    const Register *blksize = builder.getRegister();
    builder.CreateLoad(blksize, blklensR, 0, ct, 8);
    builder.CreateMul(blksize, blksize, llvmOldsize);
    const Register *t = builder.getRegister();
    builder.CreateAdd(t, copied, blksize);
    builder.CreateCondBr(builder.CreateICmpGT(t, sizeleft),
                         bbCount, bbCountTail);
    builder.clearRegister(t);

    builder.SetInsertPoint(bbCountTail);
    builder.CreateAdd(copied, copied, blksize);
    builder.clearRegister(blksize);
    builder.CreateInc(ct);
    builder.CreateCondBr(builder.CreateICmpLT(ct, llvmCount),
                         bbCountBody, bbCount);

    builder.SetInsertPoint(bbCount);
    const Register *idx  = builder.getRegister();
    builder.CreateSub(idx, llvmCount, left);
    builder.CreateCondBr(builder.CreateICmpGT(ct, idx),
                         bbSome, bbNone);

    builder.SetInsertPoint(bbSome);
    if(pack) {
        builder.CreateCall(packFn, outbase, base, idx, ct);
        builder.CreateMove(outbase, builder.ret());
    } else {
        builder.CreateCall(packFn, base, inbase, idx, ct);
        builder.CreateMove(inbase, builder.ret());
    }
    builder.CreateSub(sizeleft, sizeleft, copied);
    const Register *tmp = builder.getRegister();
    builder.CreateSub(tmp, ct, idx);
    builder.CreateSub(left, left, tmp);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.clearRegister(tmp);
    builder.clearRegister(idx);
    builder.clearRegister(copied);
    builder.clearRegister(left);
    builder.CreateCondBr(BranchType::Z, bbDone, bbNone);

    builder.SetInsertPoint(bbNone);
    const Register *blklen = builder.getRegister();
    builder.CreateLoad(blklen, blklensR, 0, ct, 8);
    builder.CreateStore(blklen, stack, disp_stack(sp+1, countLeft));
    builder.clearRegister(blklen);
    builder.clearRegister(blklensR);
    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateLoad(offset, offsetsR, 0, ct, 8);
    if(pack)
        builder.CreatePtr(inbase, base, 0, offset);
    else
        builder.CreatePtr(outbase, base, 0, offset);
    builder.CreateBr(bbInner);

    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);
    builder.clearRegister(ct);
    builder.clearRegister(base);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "if_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_indexedfinal(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();

    BasicBlock *bbPop = new BasicBlock("if_pop", fn, bbIns);
    BasicBlock *bbInner = DLJIT_contigfinal(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner)
        return nullptr;
    BasicBlock *bbPush = new BasicBlock("if_push", fn, bbInner);
    auto rvPop = DLJIT_idxfinal_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_idxfinal_push(env, sp, bbPush, bbOuter, bbInner, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

