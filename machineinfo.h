#ifndef DLJIT_MACHINEINFO_H
#define DLJIT_MACHINEINFO_H

// The point is that this file should be auto-generated for each target

// DLJIT_VECWIDTH_1_BITS is the widest vector instruction available  
// DLJIT_VECWIDTH_2_BITS is the next widest and so on ...

#define DLJIT_VECWIDTH_1_BITS 256
#define DLJIT_VECWIDTH_2_BITS 128
// Other available VECWIDTH's here


#define DLJIT_VECWIDTH_1 (DLJIT_VECWIDTH_1_BITS / 8)
#define DLJIT_VECWIDTH_2 (DLJIT_VECWIDTH_2_BITS / 8)

// Turns out that my hand-rolled memcpy is terribly slow, so I'm going to use
// the builtin memcpy for almost everything except very small types. Also
// means that I don't need to unroll
#define DLJIT_UNROLL_DEGREE 1
#define DLJIT_THRESHOLD_FULL_UNROLL 1024
#define DLJIT_THRESHOLD_USE_MEMCPY 1024

#define DLJIT_CACHE_LINE_SIZE 64

// NOTE: All latency values are in cycles
#define DLJIT_LATENCY_L1 4
#define DLJIT_LATENCY_L2 12
#define DLJIT_LATENCY_L3 37
#define DLJIT_LATENCY_MEM 230

#define DLJIT_CACHE_SIZE_L1 32768
#define DLJIT_CACHE_SIZE_L2 262144
#define DLJIT_CACHE_SIZE_L3 10485760


#define MACHINE_TRIPLE "x86_64-unknown-linux-gnu"
#define MACHINE_DATALAYOUT "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"

#define MACHINE_ARCH MACHINE_TRIPLE
#define MACHINE_CPU "corei7-avx"
// Optional machine attributes should be in a " " (ASCII 32) delimited list
#define MACHINE_ATTRS "avx2"

#endif
