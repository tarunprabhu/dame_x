#include "builder.h"

#include "util.h"

#include <iostream>
#include <sstream>

using namespace std;

static bool find(const std::vector<const Register*> vec, const Value *v) {
    for(auto e : vec)
        if(v == e)
            return true;
    return false;
}

Builder::Builder()
    : curr(nullptr), stackSizeV(nullptr), stackSize(0),
      regmap(decltype(regmap)()),
      registers(decltype(registers)()),
      sseregisters(decltype(sseregisters)()),
      avxregisters(decltype(avxregisters)()),
      tmpregisters(decltype(tmpregisters)()),
      argRegisters(vector<const Register*>(6)),
      retRegister(nullptr),
      ripRegister(nullptr),
      stackPointer(nullptr),
      basePointer(nullptr),
      calleeSaved(vector<const Register*>(5)),
      callerSaved(vector<const Register*>(7)),
      calleeIndexes(decltype(calleeIndexes)()),
      indexes(decltype(indexes)()),
      used(decltype(used)())
#ifdef DEBUG
#endif
{
    regmap[XED_REG_RAX] = XED_REG_EAX;
    regmap[XED_REG_RBX] = XED_REG_EBX;
    regmap[XED_REG_RCX] = XED_REG_ECX;
    regmap[XED_REG_RDX] = XED_REG_EDX;
    regmap[XED_REG_RDI] = XED_REG_EDI;
    regmap[XED_REG_RSI] = XED_REG_ESI;
    regmap[XED_REG_R8]  = XED_REG_R8D;
    regmap[XED_REG_R9]  = XED_REG_R9D;
    regmap[XED_REG_R10] = XED_REG_R10D;
    regmap[XED_REG_R11] = XED_REG_R11D;
    regmap[XED_REG_R12] = XED_REG_R12D;
    regmap[XED_REG_R13] = XED_REG_R13D;
    regmap[XED_REG_R14] = XED_REG_R14D;
    regmap[XED_REG_R15] = XED_REG_R15D;
    
    registers[XED_REG_RAX] = new Register(XED_REG_RAX);
    registers[XED_REG_RBX] = new Register(XED_REG_RBX);
    registers[XED_REG_RCX] = new Register(XED_REG_RCX);
    registers[XED_REG_RDX] = new Register(XED_REG_RDX);
    registers[XED_REG_RDI] = new Register(XED_REG_RDI);
    registers[XED_REG_RSI] = new Register(XED_REG_RSI);
    registers[XED_REG_RSP] = new Register(XED_REG_RSP);
    registers[XED_REG_RBP] = new Register(XED_REG_RBP);
    registers[XED_REG_R8]  = new Register(XED_REG_R8);
    registers[XED_REG_R9]  = new Register(XED_REG_R9);
    registers[XED_REG_R10] = new Register(XED_REG_R10);
    registers[XED_REG_R11] = new Register(XED_REG_R11);
    registers[XED_REG_R12] = new Register(XED_REG_R12);
    registers[XED_REG_R13] = new Register(XED_REG_R13);
    registers[XED_REG_R14] = new Register(XED_REG_R14);
    registers[XED_REG_R15] = new Register(XED_REG_R15);
    registers[XED_REG_RIP] = new Register(XED_REG_RIP);

    registers[XED_REG_EAX]  = new Register(XED_REG_EAX, 32);
    registers[XED_REG_EBX]  = new Register(XED_REG_EBX, 32);
    registers[XED_REG_ECX]  = new Register(XED_REG_ECX, 32);
    registers[XED_REG_EDX]  = new Register(XED_REG_EDX, 32);
    registers[XED_REG_EDI]  = new Register(XED_REG_EDI, 32);
    registers[XED_REG_ESI]  = new Register(XED_REG_ESI, 32);
    registers[XED_REG_R8D]  = new Register(XED_REG_R8D, 32);
    registers[XED_REG_R9D]  = new Register(XED_REG_R9D, 32);
    registers[XED_REG_R10D] = new Register(XED_REG_R10D, 32);
    registers[XED_REG_R11D] = new Register(XED_REG_R11D, 32);
    registers[XED_REG_R12D] = new Register(XED_REG_R12D, 32);
    registers[XED_REG_R13D] = new Register(XED_REG_R13D, 32);
    registers[XED_REG_R14D] = new Register(XED_REG_R14D, 32);
    registers[XED_REG_R15D] = new Register(XED_REG_R15D, 32);
    
    tmpregisters[8] = registers.at(XED_REG_RAX);
    tmpregisters[4] = registers.at(XED_REG_EAX);
    tmpregisters[2] = new Register(XED_REG_AX, 16);
    tmpregisters[1] = new Register(XED_REG_AL, 8);
                                             
    for(unsigned i = 0; i < 16; i++) {
        xed_reg_enum_t sse = XED_STATIC_CAST(xed_reg_enum_t, XED_REG_XMM0+i);
        xed_reg_enum_t avx = XED_STATIC_CAST(xed_reg_enum_t, XED_REG_YMM0+i);
        sseregisters[sse] = new Register(sse);
        avxregisters[avx] = new Register(avx);
    }
    
    argRegisters[0] = registers.at(XED_REG_RDI);
    argRegisters[1] = registers.at(XED_REG_RSI);
    argRegisters[2] = registers.at(XED_REG_RDX);
    argRegisters[3] = registers.at(XED_REG_RCX);
    argRegisters[4] = registers.at(XED_REG_R8);
    argRegisters[5] = registers.at(XED_REG_R9);

    calleeSaved[0] = registers.at(XED_REG_RBX);
    calleeSaved[1] = registers.at(XED_REG_R12);
    calleeSaved[2] = registers.at(XED_REG_R13);
    calleeSaved[3] = registers.at(XED_REG_R14);
    calleeSaved[4] = registers.at(XED_REG_R15);

    callerSaved[0] = registers.at(XED_REG_RDI);
    callerSaved[1] = registers.at(XED_REG_RSI);
    callerSaved[2] = registers.at(XED_REG_RCX);
    callerSaved[3] = registers.at(XED_REG_R8);
    callerSaved[4] = registers.at(XED_REG_R9);
    callerSaved[5] = registers.at(XED_REG_R11);
    callerSaved[6] = registers.at(XED_REG_R10);

    retRegister = registers[XED_REG_RAX];
    ripRegister = registers[XED_REG_RIP];

    stackPointer = registers[XED_REG_RSP];
    basePointer  = registers[XED_REG_RBP];
}

Builder::~Builder() {
    delete tmpregisters.at(2);
    delete tmpregisters.at(1);
    for(auto r : sseregisters)
        delete r.second;
    for(auto r : avxregisters)
        delete r.second;
    for(auto r : registers)
        delete r.second;
    if(stackSize)
        delete stackSizeV;
}

const Register* Builder::getRegister(unsigned i) {
    const Register *rv = nullptr;
    if(i == 0) {
        rv = callerSaved.at(indexes.back());
        indexes[indexes.size()-1] += 1;
        used.top().insert(rv);
    } else if(i == 1) {
        rv = calleeSaved.at(calleeIndexes.back());
        calleeIndexes[calleeIndexes.size()-1] += 1;
    }
    return rv;
}

void Builder::clearRegister(const Register *reg, unsigned i) {
    if(i == 0) {
        indexes[indexes.size()-1] -= 1;
        used.top().erase(used.top().find(reg));
    } else {
        assert(0);
    }
}

void Builder::StartFunction() {
    std::set<const Register*> newused;
    used.push(newused);
    indexes.push_back(0);
    calleeIndexes.push_back(0);
}

void Builder::EndFunction() {
    used.pop();
    indexes.pop_back();
    calleeIndexes.pop_back();
}

int Builder::StackVar(int size) {
    stackSize += size;
    return stackSize-size;
}

void Builder::StackAllocate() {
    CreatePtr(stackPointer, stackPointer, -stackSize);
}

void Builder::StackDeallocate() {
    CreatePtr(stackPointer, stackPointer, stackSize);
}

void Builder::preCall(const vector<const Value*> args) {
    // Save registers
    for(auto it = caller_begin(); it != caller_end(); it++)
        if(used.top().find(*it) != used.top().end())
            curr->append(new PushInst(*it));

    // Load argument registers 
    for(unsigned i = args.size(); i > 0; i--) {
        const Register *reg = dynamic_cast<const Register*>(args.at(i-1));
        if(find(argRegisters, reg))
            CreatePush(reg);
    }
    for(unsigned i = 0; i < args.size(); i++)
        if(find(argRegisters, args.at(i)))
            CreatePop(argRegisters.at(i));
        else 
            CreateMove(argRegisters.at(i), args.at(i));
}

void Builder::postCall(Function *f) {
    // Pop saved registers
    for(auto it = caller_rbegin(); it != caller_rend(); it++)
        if(used.top().find(*it) != used.top().end())
            CreatePop(*it);

    // Set function being called as the callee of the calling function. We
    // need this so we can do the linking in two passes
    curr->parent()->addCallee(f);
}

void Builder::createCall(Function *f, const vector<const Value*> args) {
    preCall(args);
    curr->append(new CallInst(f));
    postCall(f);
}

void Builder::createCall(LibraryFunction *f, const vector<const Value*> args) {
    preCall(args);
    const Register* reg = argRegisters.at(args.size());
    if(used.top().find(reg) != used.top().end())
        CreatePush(reg);
    CreateMove(reg, f->addr());
    curr->append(new IndirectCallInst(reg));
    if(used.top().find(reg) != used.top().end())
        CreatePop(reg);
    postCall(f);
}

void Builder::CreateLoad(const Register *dst, const Register *base, int displacement, const Register *index, byte scale) {
    curr->append(new LoadInst(dst, base, displacement, index, scale));
}

void Builder::CreateLoad(const Register *dst, const Pointer *src) {
    curr->append(new LoadInst(dst, src));
}

void Builder::CreateStore(const Register *val, const Register *base, int displacement, const Register *index, byte scale) {
    curr->append(new StoreInst(val, base, displacement, index, scale));
}

void Builder::CreateStore(const Register *val, const Pointer *dst) {
    curr->append(new StoreInst(val, dst));
}

void Builder::CreateStore(const Constant *val, const Register *base, int displacement, const Register *index, byte scale) {
    const Register *rdx = registers.at(XED_REG_RDX);
    CreateMove(rdx, val);
    curr->append(new StoreInst(rdx, base, displacement, index, scale));
}

void Builder::CreatePtr(const Register *dst, const Register *base, int displacement, const Register *index, byte scale) {
    curr->append(new PtrInst(dst, base, displacement, index, scale));
}

void Builder::CreateMove(const Register *dst, const Value *src) {
    if(dst == src)
        return;
    if(const Constant *c = dynamic_cast<const Constant*>(src)) {
        if(c->val() == 0) {
            CreateXor(dst, dst, dst);
        } else if(c->val() == 1) {
            CreateXor(dst, dst, dst);
            CreateInc(dst);
        } else if(c->val() == -1) {
            CreateXor(dst, dst, dst);
            CreateDec(dst);
        } else {
            if(dljit_bits(c->val(), 8) < 64)
                curr->append(new MoveInst(registers.at(regmap.at(dst->reg())), c));
            else
                curr->append(new MoveInst(dst, c));
        }
    } else if(const BlockAddress *b = dynamic_cast<const BlockAddress*>(src)) {
        curr->append(new MoveInst(registers.at(regmap.at(dst->reg())), b));
    } else {
        curr->append(new MoveInst(dst, src));
    }
}

void Builder::CreateMoveSignExtend(const Register *dst, const Register *src) {
  if(dst == src)
    return;
  curr->append(new MoveSignExtendInst(dst, src));
}

void Builder::CreateCondMove(BranchType type, const Register *dst, const Register *src) {
    curr->append(new CondMoveInst(type, dst, src));
}

BranchType Builder::createICmp(BranchType type, const Register *op1, const Register *op2) {
    curr->append(new CmpInst(op1, op2));
    return type;
}

BranchType Builder::createICmp(BranchType type, const Register *op1, const Constant *op2) {
    const Register *rdx = registers.at(XED_REG_RDX);
    CreateMove(rdx, op2);
    curr->append(new CmpInst(op1, rdx));
    return type;
}

BranchType Builder::CreateICmpEQ(const Register *op1, const Register *op2) {  return createICmp(BranchType::EQ, op1, op2);  }
BranchType Builder::CreateICmpEQ(const Register *op1, const Constant *op2) {  return createICmp(BranchType::EQ, op1, op2);  }

BranchType Builder::CreateICmpNE(const Register *op1, const Register *op2) {  return createICmp(BranchType::NE, op1, op2);  }
BranchType Builder::CreateICmpNE(const Register *op1, const Constant *op2) {  return createICmp(BranchType::NE, op1, op2);  }

BranchType Builder::CreateICmpGT(const Register *op1, const Register *op2) {  return createICmp(BranchType::GT, op1, op2); }
BranchType Builder::CreateICmpGT(const Register *op1, const Constant *op2) {  return createICmp(BranchType::GT, op1, op2); }

BranchType Builder::CreateICmpGE(const Register *op1, const Register *op2) {  return createICmp(BranchType::GE, op1, op2); }
BranchType Builder::CreateICmpGE(const Register *op1, const Constant *op2) {  return createICmp(BranchType::GE, op1, op2); }

BranchType Builder::CreateICmpLT(const Register *op1, const Register *op2) {  return createICmp(BranchType::LT, op1, op2); }
BranchType Builder::CreateICmpLT(const Register *op1, const Constant *op2) {  return createICmp(BranchType::LT, op1, op2); }

BranchType Builder::CreateICmpLE(const Register *op1, const Register *op2) {  return createICmp(BranchType::LE, op1, op2); }
BranchType Builder::CreateICmpLE(const Register *op1, const Constant *op2) {  return createICmp(BranchType::LE, op1, op2); }

BranchType Builder::CreateICmpZ(const Register *op) {
    curr->append(new BinaryInst(BinaryType::Test, op, op));
    return BranchType::Z;
}

BranchType Builder::CreateICmpNZ(const Register *op) {
    curr->append(new BinaryInst(BinaryType::Test, op, op));
    return BranchType::NZ;
}

void Builder::CreateBr(const BasicBlock *target) {
    curr->append(new BranchInst(BranchType::MP, target));
}

void Builder::CreateCondBr(BranchType type, const BasicBlock *_true, const BasicBlock *_false){
    curr->append(new BranchInst(type, _true));
    CreateBr(_false);
}

void Builder::CreateIndirectBr(const Register *reg) {
    curr->append(new IndirectBrInst(reg));
}

void Builder::createBinary(BinaryType type, const Register *dst, const Register *op1, const Register *op2) {
    if(dst == op1 && op1 == op2) {
        curr->append(new BinaryInst(type, dst, dst));
    } else if(dst != op2) {
        CreateMove(dst, op1);
        curr->append(new BinaryInst(type, dst, op2));
    } else {
        const Register *rdx = registers.at(XED_REG_RDX);
        CreateMove(rdx, op2);
        CreateMove(dst, op1);
        curr->append(new BinaryInst(type, dst, rdx));
    }
}

void Builder::createBinary(BinaryType type, const Register *dst, const Register *op1, const Constant *op2) {
    const Register *rdx = registers.at(XED_REG_RDX);
    CreateMove(rdx, op2);
    CreateMove(dst, op1);
    curr->append(new BinaryInst(type, dst, rdx));
}

void Builder::createBinary(BinaryType type, const Register *dst, const Constant *op1, const Register *op2) {
    if(dst != op2) {
        CreateMove(dst, op1);
        curr->append(new BinaryInst(type, dst, op2));
    } else {
        const Register *rdx = registers.at(XED_REG_RDX);
        CreateMove(rdx, op2);
        CreateMove(dst, op1);
        curr->append(new BinaryInst(type, dst, rdx));
    }
}
        
void Builder::CreateAdd(const Register *dst, const Register *op1, const Register *op2) {  createBinary(BinaryType::Add, dst, op1, op2);  }
void Builder::CreateAdd(const Register *dst, const Register *op1, const Constant *op2) {  createBinary(BinaryType::Add, dst, op1, op2);  }
void Builder::CreateAdd(const Register *dst, const Constant *op1, const Register *op2) {  createBinary(BinaryType::Add, dst, op1, op2);  }

void Builder::CreateSub(const Register *dst, const Register *op1, const Register *op2) {  createBinary(BinaryType::Sub, dst, op1, op2);  }
void Builder::CreateSub(const Register *dst, const Register *op1, const Constant *op2) {  createBinary(BinaryType::Sub, dst, op1, op2);  }
void Builder::CreateSub(const Register *dst, const Constant *op1, const Register *op2) {  createBinary(BinaryType::Sub, dst, op1, op2);  }

void Builder::CreateAnd(const Register *dst, const Register *op1, const Register *op2) {  createBinary(BinaryType::And, dst, op1, op2);  }
void Builder::CreateAnd(const Register *dst, const Register *op1, const Constant *op2) {  createBinary(BinaryType::And, dst, op1, op2);  }
void Builder::CreateAnd(const Register *dst, const Constant *op1, const Register *op2) {  createBinary(BinaryType::And, dst, op1, op2);  }

void Builder::CreateOr(const Register *dst, const Register *op1, const Register *op2) {  createBinary(BinaryType::Or, dst, op1, op2);  }
void Builder::CreateOr(const Register *dst, const Register *op1, const Constant *op2) {  createBinary(BinaryType::Or, dst, op1, op2);  }
void Builder::CreateOr(const Register *dst, const Constant *op1, const Register *op2) {  createBinary(BinaryType::Or, dst, op1, op2);  }

void Builder::CreateXor(const Register *dst, const Register *op1, const Register *op2) {  createBinary(BinaryType::Xor, dst, op1, op2);  }
void Builder::CreateXor(const Register *dst, const Register *op1, const Constant *op2) {  createBinary(BinaryType::Xor, dst, op1, op2);  }
void Builder::CreateXor(const Register *dst, const Constant *op1, const Register *op2) {  createBinary(BinaryType::Xor, dst, op1, op2);  }


void Builder::CreateMul(const Register *dst, const Register *op1, const Register *op2) {
    // TODO: FIXME: We are assuming that the result of the multiplication 
    // is small enought that it isn't split across RAX and RDX
    const Register *rax = registers.at(XED_REG_RAX);
    const Register *rdx = registers.at(XED_REG_RDX);

    CreateMove(rax, op1);
    CreateXor(rdx, rdx, rdx);
    curr->append(new UnaryInst(UnaryType::Mul, op2));
    CreateMove(dst, rax);
}

void Builder::CreateMul(const Register *dst, const Register *op1, const Constant *op2) {
    // TODO: FIXME: We are assuming that the result of the multiplication 
    // is small enought that it isn't split across RAX and RDX
    const Register *rax = registers.at(XED_REG_RAX);
    const Register *rdx = registers.at(XED_REG_RDX);
    
    // FIXME: This is just plain wrong, but I can't figure out how to get a true
    // 64-bit quantity into the register
    if(op2->val() >= 0) {
      CreateMove(rax, op2);
      CreateXor(rdx, rdx, rdx);
      curr->append(new UnaryInst(UnaryType::Mul, op1));
      CreateMove(dst, rax);
    } else {
      // For some reason that I cannot now fathom, all the immediates are b
      // being treated as 32-bit.
      // **** FIXME: **** STOP TREATING ALL IMMEDIATES AS 32-BIT
      // When the constant is negative, this is incorrect because the higher
      // order bits then become zero. To fix it, we sign extend it. This should
      // happen everywhere, but right now, there's a specific bug I want to fix
      // which happens to be as a reuslt of a multiply, so...
      CreateMove(rax, op2);
      CreateMoveSignExtend(rax, registers.at(XED_REG_EAX));
      curr->append(new UnaryInst(UnaryType::Mul, op1));
      CreateMove(dst, rax);
    }
}

void Builder::CreateDiv(const Register *dst, const Register *op1, const Register *op2) {
    const Register *rax = registers.at(XED_REG_RAX);
    const Register *rdx = registers.at(XED_REG_RDX);

    CreateMove(rax, op1);
    CreateXor(rdx, rdx, rdx);
    curr->append(new UnaryInst(UnaryType::Div, op2));
    CreateMove(dst, rax);
}

void Builder::CreateDiv(const Register *dst, const Register *op1, const Constant *op2) {
    const Register *rax = registers.at(XED_REG_RAX);
    const Register *rdx = registers.at(XED_REG_RDX);

    const Register *tmp = getRegister();
    
    CreateMove(rax, op1);
    CreateMove(tmp, op2);
    CreateXor(rdx, rdx, rdx);
    curr->append(new UnaryInst(UnaryType::Div, tmp));
    CreateMove(dst, rax);

    clearRegister(tmp);
}

void Builder::CreateRem(const Register *dst, const Register *op1, const Register *op2) {
    const Register *rax = registers.at(XED_REG_RAX);
    const Register *rdx = registers.at(XED_REG_RDX);

    CreateMove(rax, op1);
    CreateXor(rdx, rdx, rdx);
    curr->append(new UnaryInst(UnaryType::Rem, op2));
    CreateMove(dst, rdx);
}

void Builder::CreateRem(const Register *dst, const Register *op1, const Constant *op2) {
    const Register *rax = registers.at(XED_REG_RAX);
    const Register *rdx = registers.at(XED_REG_RDX);

    const Register *tmp = getRegister();
    
    CreateMove(rax, op1);
    CreateMove(tmp, op2);
    CreateXor(rdx, rdx, rdx);
    curr->append(new UnaryInst(UnaryType::Rem, tmp));
    CreateMove(dst, rdx);

    clearRegister(tmp);
}


void Builder::CreateInc(const Register *dst) {
    curr->append(new UnaryInst(UnaryType::Inc, dst));
}

void Builder::CreateDec(const Register *dst) {
    curr->append(new UnaryInst(UnaryType::Dec, dst));
}

void Builder::CreateNot(const Register *dst) {
    curr->append(new UnaryInst(UnaryType::Not, dst));
}

void Builder::CreateCall(Function *fn) {
    createCall(fn, {   });
}

void Builder::CreateCall(Function *fn, const Value *arg0) {
    createCall(fn, { arg0 });
}

void Builder::CreateCall(Function *fn, const Value *arg0, const Value *arg1) {
    createCall(fn, { arg0, arg1 });
}

void Builder::CreateCall(Function *fn, const Value *arg0, const Value *arg1, const Value *arg2) {
    createCall(fn, { arg0, arg1, arg2 });
}

void Builder::CreateCall(Function *fn, const Value *arg0, const Value *arg1, const Value *arg2, const Value *arg3) {
    createCall(fn, { arg0, arg1, arg2, arg3 });
}

void Builder::CreateCall(Function *fn, const Value *arg0, const Value *arg1, const Value *arg2, const Value *arg3, const Value *arg4) {
    createCall(fn, { arg0, arg1, arg2, arg3, arg4 });
} 

void Builder::CreateCall(LibraryFunction *fn) {
    createCall(fn, {  });
}

void Builder::CreateCall(LibraryFunction *fn, const Value *arg0) {
    createCall(fn, { arg0 });
}

void Builder::CreateCall(LibraryFunction *fn, const Value *arg0, const Value *arg1) {
    createCall(fn, { arg0, arg1 });
}

void Builder::CreateCall(LibraryFunction *fn, const Value *arg0, const Value *arg1, const Value *arg2) {
    createCall(fn, { arg0, arg1, arg2 });
}

void Builder::CreateCall(LibraryFunction *fn, const Value *arg0, const Value *arg1, const Value *arg2, const Value *arg3) {
    createCall(fn, { arg0, arg1, arg2, arg3 });
}

void Builder::CreateCall(LibraryFunction *fn, const Value *arg0, const Value *arg1, const Value *arg2, const Value *arg3, const Value *arg4) {
    createCall(fn, { arg0, arg1, arg2, arg3, arg4 });
} 


void Builder::CreateRet(const Value *val) {
    if(val)
        CreateMove(retRegister, val);
    curr->append(new ReturnInst());
}

void Builder::CreatePush(const Register *reg) {
    curr->append(new PushInst(reg));
}

void Builder::CreatePop(const Register *reg) {
    curr->append(new PopInst(reg));
}

void Builder::CreateAlignedSSELoad(const Register* dst, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new SSELoadInst(true, dst, base, disp, index, scale));
}

void Builder::CreateUnalignedSSELoad(const Register* dst, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new SSELoadInst(false, dst, base, disp, index, scale));
}

void Builder::CreateAlignedSSEStore(const Register *src, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new SSEStoreInst(true, src, base, disp, index, scale));
}

void Builder::CreateUnalignedSSEStore(const Register *src, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new SSEStoreInst(false, src, base, disp, index, scale));
}

void Builder::CreateAlignedAVXLoad(const Register* dst, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new AVXLoadInst(true, dst, base, disp, index, scale));
}

void Builder::CreateUnalignedAVXLoad(const Register* dst, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new AVXLoadInst(false, dst, base, disp, index, scale));
}

void Builder::CreateAlignedAVXStore(const Register *src, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new AVXStoreInst(true, src, base, disp, index, scale));
}

void Builder::CreateUnalignedAVXStore(const Register *src, const Register *base, int disp, const Register *index, byte scale) {
    curr->append(new AVXStoreInst(false, src, base, disp, index, scale));
}
