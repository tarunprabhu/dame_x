#include "jit.h"

#include <cstring>
#include <sstream>

using namespace std;


#ifdef DEBUG
const char *contig_pushes[] = { "contig0_push", "contig1_push", "contig2_push",
                                "contig3_push", "contig4_push", "contig5_push",
                                "contig6_push", "contig7_push", "contig8_push",
                                "contig9_push", "contig10_push","contig11_push",
                                "contig12_push", "contig13_push", "contig14_push" };
const char *contig_pops[] = { "contig0_pop", "contig1_pop", "contig2_pop",
                           "contig3_pop", "contig4_pop", "contig5_pop",
                           "contig6_pop", "contig7_pop", "contig8_pop",
                           "contig9_pop", "contig10_pop", "contig11_pop",
                           "contig12_pop", "contig13_pop", "contig14_pop" };

const char *cfs[] = { "cf0_push", "cf1_push", "cf2_push",
                      "cf3_push", "cf4_push", "cf5_push",
                      "cf6_push", "cf7_push", "cf8_push",
                      "cf9_push", "cf10_push", "cf11_push",
                      "cf12_push", "cf13_push", "cf14_push" };
#endif


static BasicBlock* DLJIT_contiguous_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbInner, bool pack) {
    const Dataloop *dl = env.dl();
    Builder& builder = env.builder();

    auto llvmCount = env.int64Const(dl[sp].count);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();

    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "contig_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, contig_pushes[sp]);
    }
#endif
    if(pack) {
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
    } else {
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
    }
    builder.CreateStore(llvmCount, stack, disp_stack(sp, countLeft));

    builder.CreateBr(bbInner);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "contig_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif

    return bb;
}

static BasicBlock* DLJIT_contiguous_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    const Dataloop *dl = env.dl();
    Builder& builder = env.builder();
    Function *fn = env.fn();

    std::stringstream ss;

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBaseExtent = env.int64Const(dl[sp].s.c_t.baseextent);

    ss.str(std::string());
    ss << "contig" << sp << "_notdone";
    BasicBlock *bbNotDone = new BasicBlock(ss.str(), fn, bbIns);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();

    /* Check if the type is done */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "contig_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, contig_pops[sp]);
    }
#endif
    const Register *left = builder.getRegister();
    
    // if(--stack[sp].countLeft == 0)
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbOuter, bbNotDone);

    /* Not done with the type */
    builder.SetInsertPoint(bbNotDone);
    const Register *base = builder.getRegister();
    const Register *idx = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateSub(idx, llvmCount, left);
    builder.CreateMul(offset, idx, llvmBaseExtent);
    if(pack) 
        builder.CreatePtr(inbase, base, 0, offset);
    else 
        builder.CreatePtr(outbase, base, 0, offset);
    builder.CreateBr(bbInner);

    builder.clearRegister(offset);
    builder.clearRegister(idx);
    builder.clearRegister(base);
    builder.clearRegister(left);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "contig_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif

    return bb;
}

BasicBlock* DLJIT_contiguous(BuildEnv& env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();
    stringstream ss;

    ss.str(string());
    ss << "contig" << sp << "_pop";
    BasicBlock *bbPop = new BasicBlock(ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) return nullptr;

    ss.str(string());
    ss << "contig" << sp << "_push";
    BasicBlock *bbPush = new BasicBlock(ss.str(), fn, bbInner);

    auto rvPop = DLJIT_contiguous_pop(env, sp, bbPop, bbOuter, bbInner, bbIns, pack);
    auto rvPush = DLJIT_contiguous_push(env, sp, bbPush, bbInner, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

BasicBlock* DLJIT_contigfinal(BuildEnv& env, int sp, BasicBlock* bbOuter, BasicBlock *bbIns, bool pack) {
    Builder& builder = env.builder();
    Module& module = env.module();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();

    long basesize = dl[sp].s.c_t.basesize;

    const Constant *llvmCount = env.int64Const(dl[sp].count);
    const Constant *llvmBasesize = env.int64Const(basesize);
    const Constant *llvmSp = env.int64Const(sp);

    auto bb            = new BasicBlock("cf", fn, bbIns);
    auto bbCanCopy     = new BasicBlock("canCopy", fn, bbIns);
    auto bbNotEnough   = new BasicBlock("notEnough", fn, bbIns);
    auto bbNotAllPartial = new BasicBlock("notAllPartial", fn, bbIns);
    auto bbAllPartial  = new BasicBlock("allPartial", fn, bbIns);
    auto bbIsPartial   = new BasicBlock("isPartial", fn, bbIns);
    auto bbPrepareSave = new BasicBlock("preparesave", fn, bbIns);
    auto bbEnough      = new BasicBlock("enough", fn, bbIns);
    auto bbSave        = new BasicBlock("save", fn, bbIns);
    
    auto memcpyFn = module.getLibraryFunction("memcpy", (void*)memcpy);
    
    const Register *stack = env.stack();
    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *sizeleft = env.sizeleft();
    const Register *partial = env.partial();

    /* Entry */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "cf(" << sp << ")";
        builder.startCheck(ss.str());
    }
#endif
    const Register *resumeAddr = builder.getRegister();
    const Register *left = builder.getRegister();
    const Register *newleft = builder.getRegister();
    
    builder.CreatePtr(resumeAddr, builder.rip());
    // The LEA instruction that is generated by the previous instruction
    // is 7 bytes long. THIS SHOULD NOT BE HARDCODED
    builder.CreateSub(resumeAddr, resumeAddr, env.int64Const(7));
#ifdef DEBUG
        DEBUG_PUTS(env, cfs[sp]);
#endif
    if(pack) {
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
    } else {
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
    }
    // if(sizeleft == 0)
    builder.CreateCondBr(builder.CreateICmpZ(sizeleft),
                         bbSave, bbCanCopy);

    // if(stack[sp].countleft == 0 and partial == 0)
    builder.SetInsertPoint(bbCanCopy);
    const Register *tmp1 = builder.getRegister();
    const Register *tmp2 = builder.getRegister();
    builder.CreateMove(tmp2, llvmCount);
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateOr(tmp1, left, partial);
    builder.CreateCondMove(BranchType::Z, left, tmp2);
    builder.clearRegister(tmp2);
    builder.clearRegister(tmp1);
    const Register *tocopy = builder.getRegister();
    builder.CreateMul(tocopy, left, llvmBasesize);
    builder.CreateAdd(tocopy, tocopy, partial);
    builder.CreateCondBr(builder.CreateICmpGT(tocopy, sizeleft),
                         bbNotEnough, bbEnough);

    // if(tocopy > sizeleft)
    builder.SetInsertPoint(bbNotEnough);
    /* TODO: Don't use memcpy for small copies */
    builder.CreateCall(memcpyFn, outbase, inbase, sizeleft);
    builder.CreateMove(newleft, left);          // newleft = stack[sp].countleft
    builder.CreateCondBr(builder.CreateICmpGE(partial, sizeleft),
                         bbNotAllPartial, bbAllPartial);

    // if(partial >= sizeleft)
    builder.SetInsertPoint(bbNotAllPartial);
    builder.CreateSub(partial, partial, sizeleft);
    builder.CreateBr(bbPrepareSave);

    // if(partial < sizeleft)
    builder.SetInsertPoint(bbAllPartial);
    const Register *tmp = builder.getRegister();
    const Register *avail = builder.getRegister();
    builder.CreateSub(avail, sizeleft, partial);   // avail = sizeleft - partial
    builder.CreateDiv(tmp, avail, llvmBasesize);
    builder.CreateSub(newleft, newleft, tmp);      // newleft -= avail/basesize
    builder.CreateMove(partial, env.int64Const(0));// partial = 0
    builder.CreateRem(tmp, avail, llvmBasesize);
    builder.CreateCondBr(builder.CreateICmpNZ(tmp),
                         bbIsPartial, bbPrepareSave);
    builder.clearRegister(avail);

    // if(avail % basesize)
    builder.SetInsertPoint(bbIsPartial);
    builder.CreateDec(newleft);
    builder.CreateSub(partial, llvmBasesize, tmp);
    builder.CreateBr(bbPrepareSave);
    builder.clearRegister(tmp);

    // stack[sp].base = (in/out)base + sizeleft;
    // stack[sp].countleft = newleft
    builder.SetInsertPoint(bbPrepareSave);
    if(pack) {
        builder.CreatePtr(inbase, inbase, 0, sizeleft);
    } else {
        builder.CreatePtr(outbase, outbase, 0, sizeleft);
    }
    builder.CreateStore(newleft, stack, disp_stack(sp, countLeft));
    builder.CreateBr(bbSave);

    // if(tocopy <= sizeleft)
    builder.SetInsertPoint(bbEnough);
    // TODO: Don't use memcpy for small sizes
    builder.CreateCall(memcpyFn, outbase, inbase, tocopy);
    builder.CreateMove(partial, env.int64Const(0));  // partial = 0
    builder.CreatePtr(inbase, inbase, 0, tocopy);    // inbase += tocopy
    builder.CreatePtr(outbase, outbase, 0, tocopy);  // outbase +- tocopy
    builder.CreateSub(sizeleft, sizeleft, tocopy);   // sizeleft -= tocopy
    builder.CreateStore(env.int64Const(0), stack, disp_stack(sp, countLeft));
    builder.CreateBr(bbOuter);

    /* Save state */
    builder.SetInsertPoint(bbSave);
    if(pack) {
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
    } else {
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
    }

    // Don't need the allocated registers any longer
    builder.clearRegister(tocopy);
    builder.clearRegister(newleft);
    builder.clearRegister(left);

    const Register *state = builder.getRegister();
    const Register *size = builder.getRegister();
    const Register *copied = builder.getRegister();
    const Register *blockaddr = builder.getRegister();

    builder.CreateLoad(state, builder.sp(), env.paramState());
    builder.CreateStore(resumeAddr, state, disp_state(dl));
    builder.CreateStore(llvmSp, state, disp_state(sp));
    builder.CreateStore(partial, state, disp_state(partial));

    builder.CreateLoad(size, builder.sp(), env.paramSize());
    builder.CreateLoad(copied, builder.sp(), env.paramCopied());
    builder.CreateStore(size, copied);
    builder.CreateStore(env.int64Const(1), builder.sp(), env.retval());
    builder.CreateBr(env.bbTail());

    // Cleanup
    builder.clearRegister(blockaddr);
    builder.clearRegister(copied);
    builder.clearRegister(size);
    builder.clearRegister(state);

    builder.clearRegister(resumeAddr);

#ifdef DEBUG
    {
        stringstream ss;
        ss << "cf(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
   
    return bb;
}

