#include "util.h"

#include "jit.h"

#include <cstring>
#include <limits.h>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <sys/mman.h>
#include <unistd.h>

using namespace std;

void* DLJIT_malloc_exec(size_t sz) {
    long pagesize = sysconf(_SC_PAGE_SIZE);
    void *p = aligned_alloc(pagesize, sz);
    if (mprotect(p, sz, PROT_READ | PROT_WRITE | PROT_EXEC)) {
        abort();
    }
    return p;
}


// Returns the minimum number of bits needed to represent the constant
unsigned dljit_bits(int64_t val, unsigned min) {
    unsigned bits = 64;
    if(SCHAR_MIN <= val and val <= SCHAR_MAX)
        bits = 8;
    else if(INT_MIN <= val and val <= INT_MAX)
        bits = 32;

    if(bits <= min)
        return min;
    if(bits <= 8) return 8;
    else if(bits <= 32) return 32;
    return 64;
}

unsigned dljit_bytes(int64_t val, unsigned min) {
    return dljit_bits(val, min*8)/8;
}

void dljit_error(string err) {
    cerr << "***ERROR: " << err << "!" << endl;
    abort();
}

bool DLJIT_gen_function_header(BuildEnv &env, BasicBlock *bb) {
    Builder &builder = env.builder();

    builder.SetInsertPoint(bb);
    builder.CreatePush(builder.bp());
    // Callee save registers 
    for(auto it = builder.callee_begin(); it != builder.callee_end(); it++)
        builder.CreatePush(*it);
    builder.CreateMove(builder.bp(), builder.sp());
    return true;
}

bool DLJIT_gen_function_footer(BuildEnv &env, BasicBlock *bb) {
    Builder &builder = env.builder();

    builder.SetInsertPoint(bb);
    for(auto it = builder.callee_rbegin(); it != builder.callee_rend(); it++)
        builder.CreatePop(*it);
    builder.CreatePop(builder.bp());
    // We assume that the function always exists through bbExit and that the
    // value of the return register has been set correctly 
    builder.CreateRet();
    return true;
}

static bool dljit_gen_copy_spill(BuildEnv &env, BasicBlock *bb, const Register *dst, const Register *src, unsigned basesize, unsigned long &pos, unsigned long &rem, bool aligned=false) {
    Builder &builder = env.builder();

    builder.SetInsertPoint(bb);
    switch(basesize) {
    case 32: {
        unsigned iterations = rem/32;
        unsigned curr = pos;
        for(unsigned i = 0; i < iterations/16; i++) {
            for(unsigned j = 0; j < 16; j++) {
                int offset = curr+j*32;
                const Register *avxreg = builder.avxReg(j);
                if(aligned)
                    builder.CreateAlignedAVXLoad(avxreg, src, offset);
                else
                    builder.CreateUnalignedAVXLoad(avxreg, src, offset);
            }
            for(unsigned j = 0; j < 16; j++) {
                const Register *avxreg = builder.avxReg(j);
                int offset = curr+j*32;
                if(aligned)
                    builder.CreateAlignedAVXStore(avxreg, dst, offset);
                else
                    builder.CreateUnalignedAVXStore(avxreg, dst, offset);
            }
            curr += 512;
        }
        for(unsigned i = 0; i < iterations%16; i++) {
            const Register *reg = builder.avxReg(i);
            int offset = curr+i*32;
            if(aligned)
                builder.CreateAlignedAVXLoad(reg, src, offset);
            else
                builder.CreateUnalignedAVXLoad(reg, src, offset);
        }
        for(unsigned i = 0; i < iterations%16; i++) {
            const Register *reg = builder.avxReg(i);
            int offset = curr+i*32;
            if(aligned)
                builder.CreateAlignedAVXStore(reg, dst, offset);
            else
                builder.CreateUnalignedAVXStore(reg, dst, offset);
        }
        rem -= iterations*32;
        pos += iterations*32;
        break;
    }
    case 16: {
        unsigned iterations = rem/16;
        unsigned curr = pos;
        for(unsigned i = 0; i < iterations/16; i++) {
            for(unsigned j = 0; j < 16; j++) {
                const Register *reg = builder.sseReg(j);
                if(aligned)
                    builder.CreateAlignedSSELoad(reg, src, curr+j*16);
                else
                    builder.CreateUnalignedSSELoad(reg, src, curr+j*16);
            }
            for(unsigned j = 0; j < 16; j++) {
                const Register *reg = builder.sseReg(j);
                if(aligned)
                    builder.CreateAlignedSSEStore(reg, dst, curr+j*16);
                else
                    builder.CreateUnalignedSSEStore(reg, dst, curr+j*16);
            }
            curr += 256;
        }
        for(unsigned i = 0; i < iterations%16; i++) {
            const Register *reg = builder.sseReg(i);
            if(aligned)
                builder.CreateAlignedSSELoad(reg, src, curr+i*16);
            else
                builder.CreateUnalignedSSELoad(reg, src, curr+i*16);
        }
        for(unsigned i = 0; i < iterations%16; i++) {
            const Register *reg = builder.sseReg(i);
            if(aligned)
                builder.CreateAlignedSSEStore(reg, dst, curr+i*16);
            else
                builder.CreateUnalignedSSEStore(reg, dst, curr+i*16);
        }
        rem -= iterations*16;
        pos += iterations*16;
        break;
    }
    case 8:
    case 4:
    case 2:
    case 1: {
        unsigned iterations = rem/basesize;
        unsigned curr = pos;
        for(unsigned i = 0; i < iterations; i++) {
            const Register *reg = builder.tmpReg(basesize);
            builder.CreateLoad(reg, src, curr);
            builder.CreateStore(reg, dst, curr);
            curr += basesize;
        }
        rem -= iterations*basesize;
        pos += iterations*basesize;
        break;
    }
    }
    return true;
}

bool dljit_gen_memcpy_n(BuildEnv &env, Function *fn, BasicBlock *bb, BasicBlock *bbExit, BasicBlock *bbIns, const Register *dst, const Register *src, unsigned long tocopy, bool aligned) {
    Builder &builder = env.builder();
    Module &module = env.module();

    auto memcpyFn = module.getLibraryFunction("memcpy", (void*)memcpy);

    builder.SetInsertPoint(bb);
#ifdef DEBUG
    builder.startCheck("dljit_gen_memcpy_n");
#endif
    const Register *preg = builder.getRegister();
    unsigned unroll = DLJIT_UNROLL_DEGREE;

    if(tocopy > DLJIT_THRESHOLD_USE_MEMCPY) {
        builder.CreateMove(preg, env.int64Const(tocopy));
        builder.CreateCall(memcpyFn, dst, src, preg);
        builder.CreateBr(bbExit);
    } else if(tocopy > DLJIT_THRESHOLD_FULL_UNROLL) {
        BasicBlock *bbBody = new BasicBlock("body", fn, bbIns);
        BasicBlock *bbTail = new BasicBlock("tail", fn, bbIns);
        BasicBlock *bbSpill = new BasicBlock("spill", fn, bbIns);

        const Register *idx = builder.getRegister();
        builder.CreateMove(idx, env.int64Const(tocopy/(32*unroll)));
        builder.CreateMove(preg, env.int64Const(0));
        builder.CreateBr(bbBody);

        builder.SetInsertPoint(bbBody);
        for(unsigned i = 0; i < unroll; i++) {
            const Register *avxreg = builder.avxReg(i%16);
            if(aligned) {
                builder.CreateAlignedAVXLoad(avxreg, src, i*32, preg);
                builder.CreateAlignedAVXStore(avxreg, dst, i*32, preg);
            } else {
                builder.CreateUnalignedAVXLoad(avxreg, src, i*32, preg);
                builder.CreateUnalignedAVXStore(avxreg, dst, i*32, preg);
            }
        }
        builder.CreateBr(bbTail);

        builder.SetInsertPoint(bbTail);
        builder.CreateAdd(preg, preg, env.int64Const(32*unroll));
        builder.CreateDec(idx);
        builder.CreateCondBr(BranchType::NZ, bbBody, bbSpill);
        builder.clearRegister(idx);

        builder.SetInsertPoint(bbSpill);
        unsigned long pos = (tocopy / (32*unroll))*(32*unroll);
        unsigned long rem = tocopy % (32*unroll);
        dljit_gen_copy_spill(env, bbSpill, dst, src, 32, pos, rem, aligned);
        dljit_gen_copy_spill(env, bbSpill, dst, src, 16, pos, rem, aligned);
        dljit_gen_copy_spill(env, bbSpill, dst, src, 8, pos, rem);
        dljit_gen_copy_spill(env, bbSpill, dst, src, 4, pos, rem);
        dljit_gen_copy_spill(env, bbSpill, dst, src, 2, pos, rem);
        dljit_gen_copy_spill(env, bbSpill, dst, src, 1, pos, rem);
        builder.CreateBr(bbExit);
    } else {
        // We can afford to use the displacement directly because the
        // threshold will almost certainly be smaller than 32 bits
        unsigned long rem = tocopy;
        unsigned long pos = 0;
        // dljit_gen_copy_spill(env, bb, dst, src, 32, pos, rem, aligned);
        dljit_gen_copy_spill(env, bb, dst, src, 16, pos, rem, aligned);
        dljit_gen_copy_spill(env, bb, dst, src, 8, pos, rem);
        dljit_gen_copy_spill(env, bb, dst, src, 4, pos, rem);
        dljit_gen_copy_spill(env, bb, dst, src, 2, pos, rem);
        dljit_gen_copy_spill(env, bb, dst, src, 1, pos, rem);
        builder.CreateBr(bbExit);
    } 

    builder.clearRegister(preg);

#ifdef DEBUG
    builder.endCheck("dljit_gen_memcpy_n");
#endif
    return true;
}

Function* DLJIT_gen_memcpy_n(BuildEnv &env, unsigned long tocopy) {
    Builder &builder = env.builder();
    Module &module = env.module();

    stringstream ss;
    ss << "DL_memcpy_" << tocopy;
    string fname = ss.str();

    Function *fn = module.getFunction(fname);
    if(fn) return fn;

    fn = module.getOrInsertFunction(fname);

    BasicBlock *bbExit = new BasicBlock("exit", fn);
    BasicBlock *bbEntry = new BasicBlock("entry", fn, bbExit);
    BasicBlock *bbAligned = new BasicBlock("aligned", fn, bbExit);
    BasicBlock *bbUnaligned = new BasicBlock("unaligned", fn, bbExit);

    builder.SetInsertPoint(bbEntry);
    builder.StartFunction();
#ifdef DEBUG
    builder.startCheck("DLJIT_gen_memcpy_n");
#endif

    // The first two registers returned by getRegister() are also the first
    // two registers through which arguments are passed in x86-64. 
    const Register *dst = builder.getRegister();
    const Register *src = builder.getRegister();

    builder.CreatePush(builder.bp());
    builder.CreateMove(builder.bp(), builder.sp());
    builder.CreateMove(dst, builder.argReg(0));
    builder.CreateMove(src, builder.argReg(1));
    const Register *tmp = builder.getRegister();
    builder.CreateOr(tmp, dst, src);
    builder.CreateAnd(tmp, tmp, env.int64Const(32-1));
    builder.clearRegister(tmp);
    builder.CreateCondBr(BranchType::Z, bbAligned, bbUnaligned);

    builder.SetInsertPoint(bbAligned);
    if(not dljit_gen_memcpy_n(env, fn, bbAligned, bbExit, bbUnaligned, dst, src, tocopy, true))
        return nullptr;

    builder.SetInsertPoint(bbUnaligned);
    if(not dljit_gen_memcpy_n(env, fn, bbUnaligned, bbExit, bbExit, dst, src, tocopy, false))
        return nullptr;
    
    builder.SetInsertPoint(bbExit);
    builder.CreateAdd(builder.ret(), dst, env.int64Const(tocopy));
    builder.CreatePop(builder.bp());
    builder.CreateRet();

    builder.clearRegister(src);
    builder.clearRegister(dst);

    builder.EndFunction();
#ifdef DEBUG
    builder.endCheck("DLJIT_gen_memcpy_n");
#endif
    return fn;
}
