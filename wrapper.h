#ifndef TLP_DATATYPES_MYJIT_WRAPPER_H
#define TLP_DATATYPES_MYJIT_WRAPPER_H

#include "module.h"
#include "types.h"

class DLJIT_wrapper {
private:
    byte *pack;
    byte *unpack;
    DLJIT_function_ptr packEntry, unpackEntry;
public:
    DLJIT_wrapper(Module &m);
    ~DLJIT_wrapper();

    DLJIT_function_ptr getPack()   const;
    DLJIT_function_ptr getUnpack() const;
};


#endif
