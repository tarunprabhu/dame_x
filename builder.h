#ifndef TLP_DATATYPES_MYJIT_BUILDER_H
#define TLP_DATATYPES_MYJIT_BUILDER_H

#include "function.h"
#include "basicblock.h"
#include "instruction.h"
#include "value.h"

#include <assert.h>
#include <map>
#include <set>
#include <stack>
#include <vector>

#ifdef DEBUG
#include <iostream>
#endif

class Builder {
private:
    BasicBlock *curr;
    Constant *stackSizeV;
    int stackSize;
    // Maps the 64-bit registers which are exclusively used by the client to
    // equivalent 32-bit registers which are used when initializing constants
    std::map<xed_reg_enum_t, xed_reg_enum_t> regmap;
    std::map<xed_reg_enum_t, const Register*> registers;
    std::map<xed_reg_enum_t, const Register*> sseregisters;
    std::map<xed_reg_enum_t, const Register*> avxregisters;
    std::map<unsigned,  const Register*> tmpregisters;
    std::vector<const Register*> argRegisters;
    const Register* retRegister;
    const Register* ripRegister;
    const Register* stackPointer;
    const Register* basePointer;
    
    std::vector<const Register*> calleeSaved;
    std::vector<const Register*> callerSaved;

    std::vector<unsigned> calleeIndexes;
    std::vector<unsigned> indexes;
    std::stack<std::set<const Register*>> used;

#ifdef DEBUG
    // This is used to keep track of the number of registers in use.
    // startCheck() must be used at the start of a function. A corresponding
    // call to endCheck must be called once the function has been generated
    // The two sets must be identical
    std::vector<unsigned> _indexes;
#endif

private:
    void createBinary(BinaryType, const Register*, const Register*, const Register*);
    void createBinary(BinaryType, const Register*, const Register*, const Constant*);
    void createBinary(BinaryType, const Register*, const Constant*, const Register*);
    BranchType createICmp(BranchType, const Register*, const Register*);
    BranchType createICmp(BranchType, const Register*, const Constant*);

    void preCall(const std::vector<const Value*> args);
    void createCall(Function *f, const std::vector<const Value*> args);
    void createCall(LibraryFunction *f, const std::vector<const Value*>);
    void postCall(Function *f);
public:
    typedef decltype(calleeSaved)::const_iterator         callee_iterator;
    typedef decltype(calleeSaved)::const_reverse_iterator callee_riterator;
    typedef decltype(callerSaved)::const_iterator         caller_iterator;
    typedef decltype(callerSaved)::const_reverse_iterator caller_riterator;
    
    Builder();
    ~Builder();

#ifdef DEBUG
    void startCheck(std::string s=std::string()) {
        _indexes.push_back(indexes.back());
    }

    void endCheck(std::string s=std::string()) {
        _indexes.pop_back();
    }
#endif

    // These functions reset the set of available registers for the function
    void StartFunction();
    void EndFunction();
    
    // This returns the next available register. Basically, we need to ensure
    // that we request all registers right at the beginning
    const Register* getRegister(unsigned=0);

    // Since we are doing everything manually here, we also have to declare 
    // that the register is now dead and can be returned to the pool
    void clearRegister(const Register*, unsigned=0);

    // Registers
    const Register* tmpReg(unsigned basesize) const {
        return tmpregisters.at(basesize);
    }
    // SSE and AVX registers
    const Register* sseReg(unsigned i) const {
        return sseregisters.at(XED_STATIC_CAST(xed_reg_enum_t, XED_REG_XMM0+i));
    }
    const Register* avxReg(unsigned i) const {
        return avxregisters.at(XED_STATIC_CAST(xed_reg_enum_t, XED_REG_YMM0+i));
    }
    
    // Gets the offset of the next stack variable that will be allocated
    int StackVar(int size);

    // Allocates the stack variables declared using StackVar
    void StackAllocate();

    // Deallocates the stack variables declared using StackVar
    void StackDeallocate();
    
    const Register* sp()  const { return stackPointer; }
    const Register* bp()  const { return basePointer; }
    const Register* ret() const { return retRegister; }
    const Register* rip() const { return ripRegister; }
    
    callee_iterator callee_begin()  const { return calleeSaved.begin();}
    callee_iterator callee_end()    const { return calleeSaved.end(); }
    callee_riterator callee_rbegin() const { return calleeSaved.rbegin(); }
    callee_riterator callee_rend()   const { return calleeSaved.rend(); }
    
    caller_iterator caller_begin()  const { return callerSaved.begin(); }
    caller_iterator caller_end()    const { return callerSaved.end(); }
    caller_riterator caller_rbegin() const { return callerSaved.rbegin(); }
    caller_riterator caller_rend()   const { return callerSaved.rend(); }

    const Register* argReg(int n) const { return argRegisters.at(n); }
    
    void SetInsertPoint(BasicBlock *bb) { curr = bb; }

    // Loads from the address saved in base to destination
    void CreateLoad(unsigned width, const Register *dst, const Register *base, int displacement=0, const Register *index=nullptr, byte scale=1);
    void CreateLoad(const Register *dst, const Register *base, int displacement=0, const Register *index=nullptr, byte scale=1);
    void CreateLoad(const Register *dst, const Pointer *src);

    // Stores the value in val to the address saved in base
    void CreateStore(unsigned width, const Register *dst, const Register *base, int displacement=0, const Register *index=nullptr, byte scale=1);
    void CreateStore(const Register *val, const Register *base, int displacement=0, const Register *index=nullptr, byte scale=1);
    void CreateStore(const Register *val, const Pointer *dst);
    void CreateStore(const Constant *val, const Register *base, int displacement=0, const Register *index=nullptr, byte scale=1);


    // Moves data from src register to dst register
    void CreateMove(const Register *dst, const Value *src);

  void CreateMoveSignExtend(const Register *dst, const Register *src);
  
    // Conditional mov
    void CreateCondMove(BranchType, const Register *dst, const Register *src);
    
    // Integer comparison intructions 
    BranchType CreateICmpEQ(const Register *op1, const Register *op2);
    BranchType CreateICmpNE(const Register *op1, const Register *op2);
    BranchType CreateICmpGT(const Register *op1, const Register *op2);
    BranchType CreateICmpGE(const Register *op1, const Register *op2);
    BranchType CreateICmpLT(const Register *op1, const Register *op2);
    BranchType CreateICmpLE(const Register *op1, const Register *op2);
    BranchType CreateICmpEQ(const Register *op1, const Constant *op2);
    BranchType CreateICmpNE(const Register *op1, const Constant *op2);
    BranchType CreateICmpGT(const Register *op1, const Constant *op2);
    BranchType CreateICmpGE(const Register *op1, const Constant *op2);
    BranchType CreateICmpLT(const Register *op1, const Constant *op2);
    BranchType CreateICmpLE(const Register *op1, const Constant *op2);

    BranchType CreateICmpZ(const Register *op);
    BranchType CreateICmpNZ(const Register *op);

    // Unconditional branch 
    void CreateBr(const BasicBlock *target);

    // Conditional branch
    void CreateCondBr(BranchType type, const BasicBlock *_true, const BasicBlock *_false);

    // Indirect branch
    void CreateIndirectBr(const Register*);
    
    // Binary operations
    void CreateAdd(const Register*, const Register*, const Register*);
    void CreateAdd(const Register*, const Register*, const Constant*);
    void CreateAdd(const Register*, const Constant*, const Register*);
    void CreateSub(const Register*, const Register*, const Register*);
    void CreateSub(const Register*, const Register*, const Constant*);
    void CreateSub(const Register*, const Constant*, const Register*);
    void CreateAnd(const Register*, const Register*, const Register*);
    void CreateAnd(const Register*, const Register*, const Constant*);
    void CreateAnd(const Register*, const Constant*, const Register*);
    void CreateOr(const Register*, const Register*, const Register*);
    void CreateOr(const Register*, const Register*, const Constant*);
    void CreateOr(const Register*, const Constant*, const Register*);
    void CreateXor(const Register*, const Register*, const Register*);
    void CreateXor(const Register*, const Register*, const Constant*);
    void CreateXor(const Register*, const Constant*, const Register*);

    void CreateMul(const Register*, const Register*, const Register*);
    void CreateMul(const Register*, const Register*, const Constant*);
    void CreateDiv(const Register*, const Register*, const Register*);
    void CreateDiv(const Register*, const Register*, const Constant*);
    void CreateRem(const Register*, const Register*, const Register*);
    void CreateRem(const Register*, const Register*, const Constant*);
   

    // Unary operations
    // The return value is usually the destination register - not quite sure 
    // why I need it to be this way
    void CreateNot(const Register*);
    void CreateInc(const Register*);
    void CreateDec(const Register*);

    // Corresponds to the LEA instruction 
    void CreatePtr(const Register *dst, const Register *base, int displacement=0, const Register *index=nullptr, byte scale=1);

    // Call Instructions
    void CreateCall(Function*);
    void CreateCall(Function*, const Value*);
    void CreateCall(Function*, const Value*, const Value*);
    void CreateCall(Function*, const Value*, const Value*, const Value*);
    void CreateCall(Function*, const Value*, const Value*, const Value*, const Value*);
    void CreateCall(Function*, const Value*, const Value*, const Value*, const Value*, const Value*);

    void CreateCall(LibraryFunction*);
    void CreateCall(LibraryFunction*, const Value*);
    void CreateCall(LibraryFunction*, const Value*, const Value*);
    void CreateCall(LibraryFunction*, const Value*, const Value*, const Value*);
    void CreateCall(LibraryFunction*, const Value*, const Value*, const Value*, const Value*);
    void CreateCall(LibraryFunction*, const Value*, const Value*, const Value*, const Value*, const Value*);

    
    // Return instruction 
    void CreateRet(const Value *val=nullptr);

    // Push/Pop
    void CreatePush(const Register *reg);
    void CreatePop(const Register *reg);

    // SSE/AVX instructions
    void CreateAlignedSSELoad(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    void CreateUnalignedSSELoad(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    void CreateAlignedSSEStore(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    void CreateUnalignedSSEStore(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);

    void CreateAlignedAVXLoad(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    void CreateUnalignedAVXLoad(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    void CreateAlignedAVXStore(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    void CreateUnalignedAVXStore(const Register*, const Register*, int=0, const Register* index=nullptr, byte=1);
    
};

#endif

