#include "dljit.h"
#include "jit.h"

#include <cstdlib>

extern "C" {
#include "xed/xed-interface.h"
#include "xed/xed-portability.h"
}

EXTERN_C void DLJIT_initialize() {
    assert(DLJIT_THRESHOLD_USE_MEMCPY < INT_MAX);
    xed_tables_init();
}

EXTERN_C void DLJIT_finalize() {
    // I don't think there's anything that needs cleanup here
}

EXTERN_C void* DLJIT_compile(void* ptr) {
    Dataloop *dl = static_cast<Dataloop*>(ptr);
    return DLJIT_compile_all(dl);
}

EXTERN_C DLJIT_function_ptr DLJIT_function_pack(void* ptr) {
    DLJIT_wrapper *wrapper = static_cast<DLJIT_wrapper*>(ptr);
    return wrapper->getPack();
}

EXTERN_C DLJIT_function_ptr DLJIT_function_unpack(void* ptr) {
    DLJIT_wrapper *wrapper = static_cast<DLJIT_wrapper*>(ptr);
    return wrapper->getUnpack();
}

EXTERN_C void DLJIT_free(void* ptr) {
    DLJIT_wrapper *wrapper = static_cast<DLJIT_wrapper*>(ptr);
    delete wrapper;
}
