#ifndef TLP_DATATYPES_MYJIT_INSTRUCTION_H
#define TLP_DATATYPES_MYJIT_INSTRUCTION_H

#include "types.h"
#include "value.h"

#include <vector>

typedef enum BranchTypesT {
    MP,     // Unconditional branch
    Z, NZ,
    GT, GE, 
    LT, LE,
    EQ, NE
} BranchType;

typedef enum BinaryTypeT {
    Add, Sub, And, Or, Xor, Cmp, Test
} BinaryType;

typedef enum UnaryTypeT {
    Inc, Dec, Not, Mul, Div, Rem
} UnaryType;


// Forward declaration to avoid circular include dependences
class BasicBlock;
class Function;

// This is meant to be an abstract class. Since I don't intend to support any
// target other than X86, I'm ok with including the XED-specific structures here
class Instruction {
protected:
    const BasicBlock *_parent;
    xed_uint8_t opcodes[XED_MAX_INSTRUCTION_BYTES];
    unsigned bytes;

    bool do_encode(xed_encoder_request_t &req, unsigned &bytes);
    void do_print(std::ostream& os, unsigned&) const;
    bool do_copy(std::vector<byte>&, unsigned &pos);
public:
    Instruction() 
        : _parent(nullptr), bytes(0) { ; }
    virtual ~Instruction() { ; }

    void setParent(const BasicBlock *parent) { _parent = parent; }
    const BasicBlock *parent() const { return _parent; }

    virtual bool pass1(unsigned&) = 0;
    // In the second pass only branch, call and placeholder instructions need to
    // do something. 
    virtual bool pass2(std::vector<byte>&, unsigned &pos);

    virtual void gas(std::ostream&, unsigned&, int=0) const = 0;
};


class BranchInst : public Instruction {
private:
    BranchType _type; 
    const BasicBlock *_target;
public:
    BranchInst(BranchType _btype, const BasicBlock *target)
        : _type(_btype), _target(target) { 
        ;
    }
    ~BranchInst() { ; }

    BranchType type()          const { return _type; }
    const BasicBlock* target() const { return _target; }
    
    virtual bool pass1(unsigned&); 
    virtual bool pass2(std::vector<byte>&, unsigned &pos);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};


class CmpInst : public Instruction {
private:
    const Register *_lhs;
    const Value *_rhs;
public:
    CmpInst(const Register *lhs, const Value *rhs)
        : _lhs(lhs), _rhs(rhs) {
        ;
    }
    ~CmpInst() { ; }

    const Register* lhs() const { return _lhs; }
    const Value*    rhs() const { return _rhs; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};


class LoadInst : public Instruction { 
private:
    const Register *_dst;
    const Pointer *_src;
public:
    LoadInst(const Register *dst, const Register *base, int disp, const Register *index, byte scale)
        : _dst(dst), _src(new Pointer(base, disp, index, scale)) {  ;  }
    LoadInst(const Register *dst, const Pointer *src)
        : _dst(dst), _src(src) { ; } 
    ~LoadInst() { 
        delete _src; 
    }

    const Register *dst()    const { return _dst; }
    const Register *base()   const { return _src->base(); }
    int displacement()       const { return _src->displacement(); }
    const Register *index()  const { return _src->index(); }
    byte scale()             const { return _src->scale(); }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class StoreInst : public Instruction { 
private:
    const Pointer *_dst;
    const Value *_src;
public:
    StoreInst(const Value *src, const Register *base, int disp, const Register *index, byte scale)
        : _dst(new Pointer(base, disp, index, scale)), _src(src) {  ;  }
    StoreInst(const Value *src, const Pointer *dst)
        : _dst(dst), _src(src) {  ;  }
    ~StoreInst() { 
        delete _dst;
    }

    const Value *src()       const { return _src; }
    const Register *base()   const { return _dst->base(); }
    int displacement()       const { return _dst->displacement(); }
    const Register *index()  const { return _dst->index(); }
    byte scale()             const { return _dst->scale(); }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class MoveInst : public Instruction { 
private:
    const Register *_dst;
    const Value *_src;
public:
    MoveInst(const Register *dst, const Value *src)
        : _dst(dst), _src(src) {
        ;
    }
    ~MoveInst() { }
    
    const Register* dst() const { return _dst; }
    const Value*    src() const { return _src; }

    virtual bool pass1(unsigned&);
    virtual bool pass2(std::vector<byte>&, unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class MoveSignExtendInst : public Instruction {
private:
  const Register *_dst;
  const Register *_src;
public:
  MoveSignExtendInst(const Register *dst, const Register *src)
    : _dst(dst), _src(src) {
    ;
  }
  ~MoveSignExtendInst() { }

  const Register* dst() const { return _dst; }
  const Register* src() const { return _src; }

  virtual bool pass1(unsigned &);
  virtual void gas(std::ostream&, unsigned&, int=0) const;
};


class CondMoveInst : public Instruction {
private:
    BranchType _type;
    const Register *_dst;
    const Register *_src;
public:
    CondMoveInst(BranchType type, const Register *dst, const Register *src)
        : _type(type), _dst(dst), _src(src) {
        ;
    }
    ~CondMoveInst() { } 
    
    BranchType     type() const { return _type; }
    const Register *dst() const { return _dst; }
    const Register *src() const { return _src; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class PtrInst : public Instruction { 
private:
    const Register *_dst;
    const Pointer *_src;
public:
    PtrInst(const Register *dst, const Register *base, int disp, const Register *index, byte scale)
        : _dst(dst), _src(new Pointer(base, disp, index, scale)) {
        ;
    }
    ~PtrInst() {
        delete _src;
    }

    const Register *dst()    const { return _dst; }
    const Register *base()   const { return _src->base(); }
    int displacement()       const { return _src->displacement(); }
    const Register *index()  const { return _src->index(); }
    byte scale()             const { return _src->scale(); }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};


class PushInst : public Instruction { 
private:
    const Register *_reg;
public:
    PushInst(const Register *reg) 
        : _reg(reg) { ; }
    ~PushInst() { ; }
    
    const Register* reg() const { return _reg; }
    
    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class PopInst : public Instruction { 
private:
    const Register *_reg;
public:
    PopInst(const Register *reg) 
        : _reg(reg) { ; }
    ~PopInst() { ; }

    const Register* reg() const { return _reg; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class BinaryInst : public Instruction { 
private:
    // In x86, we can't add an 8-bit immediate to a 64-bit register. To make
    // life "easier", I'm going to operate on registers exclusively
    BinaryType _type;
    const Register *_op1;
    const Register *_op2;
public:
    BinaryInst(BinaryType type, const Register *op1, const Register *op2) 
        : _type(type), _op1(op1), _op2(op2) {
        ;
    }
    ~BinaryInst() { ; }

    BinaryType type()  const { return _type; }
    const Register* op1() const { return _op1; }
    const Register* op2() const { return _op2; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class UnaryInst : public Instruction {
private:
    UnaryType _type;
    const Value *_op;
public:
    UnaryInst(UnaryType type, const Value *op) 
        : _type(type), _op(op) { ; }
    ~UnaryInst() { ; }

    const Value *op() const { return _op; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class ReturnInst : public Instruction { 
private:
public:
    ReturnInst() { ; }
    ~ReturnInst() { ; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

// We assume that all call instructions will be indirect through a register
// It's almost certainly going to be more efficient to do it some other way
// but since we don't expect to be making too many function calls within the 
// dataloop processing, it's ok for now. 
class CallInst : public Instruction { 
private:
    Function *_f;
public:
    CallInst(Function *f)
        : _f(f) { ; }
    ~CallInst() { ; }

    Function* function() const { return _f; }
    
    virtual bool pass1(unsigned&);
    virtual bool pass2(std::vector<byte>&, unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

// This is for indirect branches through a register. Not quite the same as
// indirect calls through a register
class IndirectBrInst : public Instruction {
private:
    const Register *_reg;
public:
    IndirectBrInst(const Register *reg)
        : _reg(reg) { ; }
    ~IndirectBrInst() { ; }

    const Register *reg() const { return _reg; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

// This is for indirect calls through a register
class IndirectCallInst : public Instruction {
private:
    const Register *_reg;
public:
    IndirectCallInst(const Register *reg)
        : _reg(reg) { ; }
    ~IndirectCallInst() { ; }
    
    const Register *reg() const { return _reg; }
    
    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};


class SSELoadInst : public Instruction {
private:
    bool _aligned;
    const Register *_dst;
    const Pointer *_src;
public:
    SSELoadInst(bool aligned, const Register *dst, const Register *base, int disp=0, const Register *index=nullptr, int scale=1)
        : _aligned(aligned), _dst(dst), _src(new Pointer(base, disp, index, scale)) { ; }
    ~SSELoadInst() { delete _src; }

    bool aligned() const { return _aligned; }
    const Register* dst() const { return _dst; }
    const Pointer*  src() const { return _src; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class SSEStoreInst : public Instruction {
private:
    bool _aligned;
    const Pointer *_dst;
    const Register *_src;
public:
    SSEStoreInst(bool aligned, const Register *src, const Register *base, int disp=0, const Register *index=nullptr, int scale=1)
        : _aligned(aligned), _dst(new Pointer(base, disp, index, scale)), _src(src) { ; }
    ~SSEStoreInst() { delete _dst; }

    bool aligned() const { return _aligned; }
    const Pointer*  dst()  const { return _dst; }
    const Register* src() const { return _src; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class AVXLoadInst : public Instruction {
private:
    bool _aligned;
    const Register *_dst;
    const Pointer *_src;
public:
    AVXLoadInst(bool aligned, const Register *dst, const Register *base, int disp=0, const Register *index=nullptr, int scale=1)
        : _aligned(aligned), _dst(dst), _src(new Pointer(base, disp, index, scale)) { ; }
    ~AVXLoadInst() { delete _src; }

    bool aligned() const { return _aligned; }
    const Register* dst() const { return _dst; }
    const Pointer* src() const { return _src; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

class AVXStoreInst : public Instruction {
private:
    bool _aligned;
    const Pointer *_dst;
    const Register *_src;
public:
    AVXStoreInst(bool aligned, const Register *src, const Register *base, int disp=0, const Register *index=nullptr, int scale=1)
        : _aligned(aligned), _dst(new Pointer(base, disp, index, scale)), _src(src) { ; }
    ~AVXStoreInst() { delete _dst; }

    bool aligned() const { return _aligned; }
    const Pointer*  dst() const { return _dst; }
    const Register* src() const { return _src; }

    virtual bool pass1(unsigned&);
    virtual void gas(std::ostream&, unsigned&, int=0) const;
};

#endif
