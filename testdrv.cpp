#include <iostream>
#include <sstream>
#include <stdint.h>

#include <iomanip>
#include <map>
#include <vector>

#include <cstring>
#include <cstdlib>

using namespace std;

#include "mpi.h"
#include "dataloop.h"

#include "jit.h"
#include "dljit.h"
#include "builder.h"
#include "util.h"

typedef int type;
typedef void*(*fnPtr)(void*,void*);

int main(int argc, char *argv[]) {
    DLJIT_initialize();

    int count = 25;
    type *dst = (type*)malloc(sizeof(type)*count);
    type *src = (type*)malloc(sizeof(type)*count);
    memset(dst, 0, sizeof(type)*count);
    for(int i = 0; i < count; i++)
        src[i] = (i+1);

    DLOOP_Dataloop dl[4];
    dl[0].kind = DL_EXIT;
    dl[1].kind = DL_VECTORFINAL;
    dl[1].count = 3;
    dl[1].size = 60;
    dl[1].extent = 76;
    dl[1].s.v_t.blklen = 5;
    dl[1].s.v_t.stride = 28;
    dl[1].s.v_t.oldsize = 4;
    dl[2].kind = DL_CONTIGFINAL;
    dl[2].count = 5;
    dl[2].size = 20;
    dl[2].extent = 20;
    dl[2].s.c_t.basesize = 4;
    dl[2].s.c_t.baseextent = 4;
    dl[3].kind = DL_BOTTOM;

    DLJIT_wrapper *w = (DLJIT_wrapper*)DLJIT_compile(dl);
    DLJIT_function_ptr fnPack = DLJIT_function_pack(w);
    DLJIT_function_ptr fnUnpack = DLJIT_function_unpack(w);

    DLOOP_Dataloop_state state;
    state.sp = 0;
    state.dl = 0;
    state.partial = 0;
    memset(state.stack, 
           0, 
           DLOOP_MAX_DATALOOP_STACK*sizeof(DLOOP_Dataloop_stackelm));

    cout << hex << "src: " << (void*)src << endl;
    cout << hex << "dst: " << (void*)dst << endl;

    long copied = 0;
    cout << "Here" << endl;
    int rv = fnPack(src, dst, count*sizeof(type), &state, &copied);
    cout << "rv: " << rv << endl;
    
    cout << dec;
    for(int i = 0; i < count; i++)
        cout << (int)dst[i] << " ";
    cout << endl;

    state.sp = 0;
    state.dl = 0;
    state.partial = 0;
    memset(state.stack, 
           0, 
           DLOOP_MAX_DATALOOP_STACK*sizeof(DLOOP_Dataloop_stackelm));
    memset(dst, 0, sizeof(type)*count);
    rv = fnUnpack(src, dst, count*sizeof(type), &state, &copied);
    cout << "rv: " << rv << endl;
    cout << dec;
    for(int i = 0; i < count; i++)
        cout << (int)dst[i] << " ";
    cout << endl;
    
    
    free(src);
    free(dst);

    DLJIT_free(w);

    DLJIT_finalize();

    return 0;
}
