#include "value.h"

#include "util.h"

#include <iostream>
#include <sstream>

using namespace std;

void Register::do_encode(xed_encoder_request_t &req, unsigned short i) const {
    xed_operand_enum_t r=XED_STATIC_CAST(xed_operand_enum_t,XED_OPERAND_REG0+i);
    xed_encoder_request_set_reg(&req, r, reg());
    xed_uint_t idx = xed_encoder_request_operand_order_entries(&req);
    xed_encoder_request_set_operand_order(&req, idx, r);
}

void Register::encode(xed_encoder_request_t &req, unsigned minbits) const { 
    do_encode(req, 0);
}

void Register::encode(xed_encoder_request_t &req, EncodeMetadata &metadata, unsigned minbits) const {
    do_encode(req, metadata.regidx);
    metadata.regidx += 1;
}

string Register::gas() const { 
    stringstream ss;
    ss << "%";
    for(auto c : name())
        ss << (char)tolower(c);
    return ss.str();
}


void Constant::encode(xed_encoder_request_t &req, unsigned minbits) const {
  if(_val < 0 and dljit_bits(_val, minbits) == 32)
    xed_encoder_request_set_simm(&req, (xed_int32_t)_val, 4);
  else if(_val >= 0)
    xed_encoder_request_set_uimm0_bits(&req, _val, dljit_bits(_val, minbits));
  else
    dljit_error("Got 64-bit negative value");
    xed_uint_t idx = xed_encoder_request_operand_order_entries(&req);
    xed_encoder_request_set_operand_order(&req, idx, XED_OPERAND_IMM0);
}

void Constant::encode(xed_encoder_request_t &req, EncodeMetadata &metadata, unsigned minbits) const {
    if(metadata.immidx != 0) 
        dljit_error("Too many immediate operands");
    encode(req, minbits);
    metadata.immidx += 1;
}

string Constant::gas() const { 
    stringstream ss;
    ss << "$0x" << hex << (int64_t)_val;
    return ss.str();
}


void Displacement::encode(xed_encoder_request_t &req, unsigned minbits) const { 
    xed_encoder_request_set_relbr(&req);
    int32_t d = XED_STATIC_CAST(xed_int32_t, _val);
    xed_encoder_request_set_branch_displacement(&req, d, dljit_bytes(d,4));
    xed_uint_t idx = xed_encoder_request_operand_order_entries(&req);
    xed_encoder_request_set_operand_order(&req, idx, XED_OPERAND_RELBR);
}

void Displacement::encode(xed_encoder_request_t &req, EncodeMetadata &metadata, unsigned minbits) const { 
    encode(req, minbits);
}


unsigned BlockAddress::addr() const { return _bb->addr(); }

void BlockAddress::encode(xed_encoder_request_t &req, unsigned minbits) const {
    xed_encoder_request_set_uimm0_bits(&req, _bb->addr(), 32);
    xed_uint_t idx = xed_encoder_request_operand_order_entries(&req);
    xed_encoder_request_set_operand_order(&req, idx, XED_OPERAND_IMM0);
}

void BlockAddress::encode(xed_encoder_request_t &req, EncodeMetadata &metadata, unsigned minbits) const {
    if(metadata.immidx != 0)
        dljit_error("Too many immediate operands");
    encode(req, minbits);
    metadata.immidx += 1;
}

string BlockAddress::gas() const { 
    stringstream ss;
    ss << "$0x" << hex << (uint32_t)_bb->addr();
    return ss.str();
}


void Pointer::encode(xed_encoder_request_t &req, bool islookup) const { 
    if(islookup) 
        xed_encoder_request_set_mem0(&req);
    else 
        xed_encoder_request_set_agen(&req);

    xed_reg_enum_t base = _base->reg();
    xed_encoder_request_set_base0(&req, base);
    xed_reg_enum_t index = XED_REG_INVALID;
    if(_index)
        index = _index->reg();
    xed_reg_enum_t segment = XED_REG_INVALID;
    xed_encoder_request_set_index(&req, index);
    xed_encoder_request_set_scale(&req, XED_STATIC_CAST(xed_uint8_t, _scale));
    if(islookup)
        xed_encoder_request_set_memory_displacement(&req, _disp, dljit_bytes(_disp, 1));
    else
        xed_encoder_request_set_memory_displacement(&req, _disp, dljit_bytes(_disp, 1));
    xed_encoder_request_set_seg0(&req, segment);

    xed_uint_t idx = xed_encoder_request_operand_order_entries(&req);
    if(islookup)
        xed_encoder_request_set_operand_order(&req, idx, XED_OPERAND_MEM0);
    else 
        xed_encoder_request_set_operand_order(&req, idx, XED_OPERAND_AGEN);
}

void Pointer::encode(xed_encoder_request_t &req, unsigned minbits) const { 
    encode(req, true);
}

void Pointer::encode(xed_encoder_request_t &req, EncodeMetadata &metadata, unsigned minbits) const {
    if(metadata.memidx != 0)
        dljit_error("Too many memory operands");
    encode(req, metadata.islookup);
    metadata.memidx += 1;
}

string Pointer::gas() const { 
    stringstream ss;
    if(_disp != 0)
        ss << "0x" << hex << (int)_disp;
    ss << "(" << _base->gas();
    if(_index)
        ss << ", " << _index->gas() << ", " << (int)_scale;
    ss << ")";
    return ss.str();
}
