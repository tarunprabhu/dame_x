#ifndef TLP_DATATYPES_MYJIT_VALUE_H
#define TLP_DATATYPES_MYJIT_VALUE_H

#include "types.h"

#include <string>

unsigned dljit_bits(int64_t, unsigned);

extern "C" {
#include "xed/xed-interface.h"
#include "xed/xed-portability.h"
}

class BasicBlock;

class Value {
protected: 
public:
    Value() { ; }
    virtual ~Value() { ; }

    virtual void encode(xed_encoder_request_t&, unsigned=0) const = 0;
    virtual void encode(xed_encoder_request_t&, EncodeMetadata&, unsigned=0) const = 0;

    virtual unsigned width()   const = 0; 
    
    virtual std::string gas()  const = 0;
};

// Simply a register
class Register : public Value { 
private:
    xed_reg_enum_t _reg;
    byte _width;

    void do_encode(xed_encoder_request_t &req, unsigned short i) const;
public:
    Register(xed_reg_enum_t r, byte width=64)
        : _reg(r), _width(width) { ; }
    ~Register() { ; }

    std::string name()         const { return xed_reg_enum_t2str(_reg); }
    xed_reg_enum_t reg()       const { return _reg; }
    virtual unsigned width()   const { return _width; }

    virtual void encode(xed_encoder_request_t &req, unsigned=0) const;
    virtual void encode(xed_encoder_request_t &req, EncodeMetadata&, unsigned=0) const;

    virtual std::string gas()  const;
};


class Constant : public Value {
protected:
    int64_t _val;
    bool _sign;

public:
    Constant(int64_t val, unsigned numbits=64, bool sign=false) 
        : _val(val), _sign(sign) { 
        ;
    }
    ~Constant() { 
        ;
    }
    
    int64_t val()            const { return _val; }
    bool sign()              const { return _sign; }
    virtual unsigned width() const { return dljit_bits(_val, 32); }

    virtual void encode(xed_encoder_request_t &req, unsigned=0) const;
    virtual void encode(xed_encoder_request_t &req, EncodeMetadata&, unsigned=0) const;

    virtual std::string gas() const;
};

class Displacement : public Constant { 
protected:
public:
    Displacement(int32_t disp)
        : Constant(disp, 32, true) { ; }
    ~Displacement() { ; }

    virtual void encode(xed_encoder_request_t &req, unsigned=0) const;
    virtual void encode(xed_encoder_request_t &req, EncodeMetadata&, unsigned=0) const;
};


class BlockAddress : public Value {
private:
    BasicBlock *_bb;
public:
    BlockAddress(BasicBlock *bb)
        : _bb(bb) { ; }
    ~BlockAddress() { ; }

    unsigned addr() const;
    virtual unsigned width() const { return 32; }
    
    virtual void encode(xed_encoder_request_t &req, unsigned=0) const;
    virtual void encode(xed_encoder_request_t &req, EncodeMetadata&, unsigned=0) const;

    virtual std::string gas() const;
};


class Pointer : public Value {
protected:
    const Register *_base;
    int _disp;
    const Register *_index;
    byte _scale;

    void encode(xed_encoder_request_t &req, bool islookup) const;
public:
    Pointer(const Register *base, int disp=0, const Register *index=nullptr, byte scale=1) 
        : _base(base), _disp(disp), _index(index), _scale(scale) {}
    ~Pointer() { ; }

    const Register* base()  const { return _base; }
    int displacement()      const { return _disp; }
    const Register* index() const { return _index; }
    byte scale()            const { return _scale; }

    virtual unsigned width() const { return 64; }
    virtual void encode(xed_encoder_request_t &req, unsigned=0) const;
    virtual void encode(xed_encoder_request_t &req, EncodeMetadata&, unsigned=0) const;

    virtual std::string gas() const;
};

#endif
