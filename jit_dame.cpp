// Code for stuff that is specific to DAME 

#include "jit.h"

#include <sstream>

using namespace std;


#ifdef DEBUG
const char *cc_pushes[] = { "cc0_push", "cc1_push", "cc2_push",
                             "cc3_push", "cc4_push", "cc5_push",
                             "cc6_push", "cc7_push", "cc8_push",
                             "cc9_push", "cc10_push", "cc11_push",
                             "cc12_push", "cc13_push", "cc14_push" };
const char *cc_pops[] = { "cc0_pop", "cc1_pop", "cc2_pop",
                           "cc3_pop", "cc4_pop", "cc5_pop",
                           "cc6_pop", "cc7_pop", "cc8_pop",
                           "cc9_pop", "cc10_pop", "cc11_pop",
                           "cc12_pop", "cc13_pop", "cc14_pop" };
#endif


static BasicBlock* DLJIT_contigchild_push(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();

    /* Push */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "cc_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, cc_pushes[sp]);
    }
#endif
    builder.CreateBr(bbInner);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "cc_push(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif

    return bb;
}

static BasicBlock* DLJIT_contigchild_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, BasicBlock *bbInner, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    stringstream ss;

    long extent = dl[sp].extent;
    
    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();

    const Constant *llvmExtent = env.int64Const(extent);
    
    ss.str(string());
    ss << "cc" << sp << "_done";
    BasicBlock *bbDone = new BasicBlock(ss.str(), fn, bbIns);
    
    /* Pop */
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "cc_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, cc_pops[sp]);
    }
#endif
    const Register *left = builder.getRegister();
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.clearRegister(left);
    builder.CreateCondBr(BranchType::Z, bbDone, bbInner);
    
    // Done
    builder.SetInsertPoint(bbDone);
    const Register *base = builder.getRegister();
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    if(pack)
        builder.CreateAdd(inbase, base, llvmExtent);
    else
        builder.CreateAdd(outbase, base, llvmExtent);
    builder.clearRegister(base);
    builder.CreateBr(bbOuter);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "cc_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif

    return bb;
}

BasicBlock* DLJIT_contigchild(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();
    stringstream ss;

    ss.str(string());
    ss << "cc" << sp << "_pop";
    BasicBlock *bbPop = new BasicBlock(ss.str(), fn, bbIns);

    BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(!bbInner) 
        return nullptr;

    ss.str(string());
    ss << "cc" << sp << "_push";
    BasicBlock *bbPush = new BasicBlock(ss.str(), fn, bbInner);

    auto rvPop = DLJIT_contigchild_pop(env,sp,bbPop,bbOuter,bbInner,bbIns,pack);
    auto rvPush = DLJIT_contigchild_push(env, sp, bbPush, bbInner,bbInner,pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}

