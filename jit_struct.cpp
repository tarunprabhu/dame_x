#include "jit.h"

#include <sstream>

using namespace std;

#ifdef DEBUG
const char* s_pushes[] = { "struct0_push", "struct1_push", "struct2_push",
                           "struct3_push", "struct4_push", "struct5_push",
                           "struct6_push", "struct7_push", "struct8_push" };
const char* s_pops[] = { "struct0_pop", "struct1_pop", "struct2_pop",
                         "struct3_pop", "struct4_pop", "struct5_pop",
                         "struct6_pop", "struct7_pop", "struct8_pop" };
#endif


static BasicBlock *DLJIT_struct_push(BuildEnv &env, int sp, BasicBlock *bb, unsigned jtidx, bool pack) {
    Builder &builder = env.builder();
    const Dataloop *dl = env.dl();
    
    long count = dl[sp].count;
    long offset0 = dl[sp].s.s_t.offsets[0];

    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmOffset0 = env.int64Const(offset0);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
    
    builder.SetInsertPoint(bb);
#ifdef DEBUG
    {
        stringstream ss;
        ss << "struct_push(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, s_pushes[sp]);
    }
#endif
    if(pack) {
        builder.CreateStore(inbase, stack, disp_stack(sp, base));
        builder.CreateAdd(inbase, inbase, llvmOffset0);
    } else {
        builder.CreateStore(outbase, stack, disp_stack(sp, base));
        builder.CreateAdd(outbase, outbase, llvmOffset0);
    }
    builder.CreateStore(llvmCount, stack, disp_stack(sp, countLeft));

    const Register *jtr = builder.getRegister();
    const Register *target = builder.getRegister();
    builder.CreateLoad(jtr, builder.sp(), env.jtaddr());
    builder.CreateLoad(target, jtr, jtidx*sizeof(uint64_t));
    builder.CreatePtr(target, jtr, 0, target);
    builder.CreateIndirectBr(target);
    builder.clearRegister(target);
    builder.clearRegister(jtr);
    
#ifdef DEBUG
    {
        stringstream ss;
        ss << "struct_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    
    return bb;
}

static BasicBlock *DLJIT_struct_pop(BuildEnv &env, int sp, BasicBlock *bb, BasicBlock *bbOuter, unsigned jtidx, BasicBlock *bbIns, bool pack) {
    Builder &builder = env.builder();
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    stringstream ss;

    long extent = dl[sp].extent;
    long count = dl[sp].count;

    const Constant *llvmCount = env.int64Const(count);
    const Constant *llvmExtent = env.int64Const(extent);
    const Constant *llvmOffsets = env.int64Const((long)dl[sp].s.s_t.offsets);

    ss.str(string());
    ss << "struct" << sp << "_done";
    BasicBlock *bbDone = new BasicBlock(ss.str(), fn, bbIns);

    ss.str(string());
    ss << "struct" << sp << "_notdone";
    BasicBlock *bbNotDone = new BasicBlock(ss.str(), fn, bbIns);

    const Register *inbase = env.inbase();
    const Register *outbase = env.outbase();
    const Register *stack = env.stack();
    
    /* Entry */
    builder.SetInsertPoint(bb);    
#ifdef DEBUG
    {
        stringstream ss;
        ss << "struct_pop(" << sp << ")";
        builder.startCheck(ss.str());
        DEBUG_PUTS(env, s_pops[sp]);
    }
#endif
    const Register *base = builder.getRegister();
    const Register *left = builder.getRegister();
    builder.CreateLoad(base, stack, disp_stack(sp, base));
    builder.CreateLoad(left, stack, disp_stack(sp, countLeft));
    builder.CreateDec(left);
    builder.CreateStore(left, stack, disp_stack(sp, countLeft));
    builder.CreateCondBr(BranchType::Z, bbDone, bbNotDone);

    builder.SetInsertPoint(bbDone);
    if(pack)
        builder.CreateAdd(inbase, base, llvmExtent);
    else
        builder.CreateAdd(outbase, base, llvmExtent);
    builder.CreateBr(bbOuter);

    builder.SetInsertPoint(bbNotDone);
    const Register *idx = builder.getRegister();
    builder.CreateSub(idx, llvmCount, left);

    const Register *offsetsR = builder.getRegister();
    const Register *offset = builder.getRegister();
    builder.CreateMove(offsetsR, llvmOffsets);
    builder.CreateLoad(offset, offsetsR, 0, idx, 8);
    if(pack)
        builder.CreatePtr(inbase, base, 0, offset);
    else
        builder.CreatePtr(outbase, base, 0, offset);
    builder.clearRegister(offset);
    builder.clearRegister(offsetsR);

    const Register *jtr = builder.getRegister();
    const Register *target = builder.getRegister();
    builder.CreateLoad(jtr, builder.sp(), env.jtaddr());
    builder.CreateLoad(target, jtr, jtidx*sizeof(uint64_t), idx, sizeof(uint64_t));
    builder.CreatePtr(target, jtr, 0, target);
    builder.CreateIndirectBr(target);
    builder.clearRegister(target);
    builder.clearRegister(jtr);
    
    builder.clearRegister(idx);
    builder.clearRegister(left);
    builder.clearRegister(base);
    
#ifdef DEBUG
    {
        stringstream ss;
        ss << "struct_pop(" << sp << ")";
        builder.endCheck(ss.str());
    }
#endif
    return bb;
}

BasicBlock* DLJIT_struct(BuildEnv &env, int sp, BasicBlock *bbOuter, BasicBlock *bbIns, bool pack) {
    Function *fn = env.fn();
    const Dataloop *dl = env.dl();
    stringstream ss;

    long count = dl[sp].count;

    ss.str(string());
    ss << "struct" << sp << "_pop";
    BasicBlock *bbPop = new BasicBlock(ss.str(), fn, bbIns);

    unsigned jtidx = fn->jtexpand(count);
    env.setDataloop(dl[sp].s.s_t.dls[0]);
    BasicBlock *bbFirstInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
    if(not(bbFirstInner and fn->jtadd(bbFirstInner, jtidx)))
        return nullptr;
    
    for(int i = 1; i < count; i++) {
        env.setDataloop(dl[sp].s.s_t.dls[i]);
        BasicBlock *bbInner = DLJIT_gen_loop(env, sp+1, bbPop, bbPop, pack);
        if(not (bbInner and fn->jtadd(bbInner, jtidx+i)))
            return nullptr;
    }
    env.setDataloop(dl);

    ss.str(string());
    ss << "struct" << sp << "_push";
    BasicBlock *bbPush = new BasicBlock(ss.str(), fn, bbFirstInner);

    auto rvPop = DLJIT_struct_pop(env, sp, bbPop, bbOuter, jtidx, bbIns, pack);
    auto rvPush = DLJIT_struct_push(env, sp, bbPush, jtidx, pack);
    if(rvPop and rvPush)
        return bbPush;
    return nullptr;
}
