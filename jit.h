#ifndef TLP_DATATYPES_MYJIT_JIT_H
#define TLP_DATATYPES_MYJIT_JIT_H

#include "mpi.h"
#include "dataloop.h"
#include "types.h"

#include "buildenv.h"

#include <assert.h>
#include <stddef.h>

#include <map>
#include <vector>

#ifdef DEBUG
#include <iostream>
#include "debug.h"
#endif

#include "basicblock.h"
#include "function.h"
#include "value.h"
#include "buildenv.h"
#include "builder.h"
#include "machineinfo.h"
#include "wrapper.h"

#define PARAM_IN 0
#define PARAM_OUT 1
#define PARAM_SIZE 2
#define PARAM_STATE 3
#define PARAM_COPIED 4

DLJIT_wrapper* DLJIT_compile_all(Dataloop *dl);

/* Code generation functions */
BasicBlock* DLJIT_gen_loop(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);

BasicBlock* DLJIT_contiguous(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_vector(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_blockindexed(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_indexed(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_contigfinal(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_vectorfinal(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_blockindexedfinal(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_indexedfinal(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_struct(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);
BasicBlock* DLJIT_contigchild(BuildEnv&, int, BasicBlock*, BasicBlock*, bool);

Function* DLJIT_gen_vecfinal(BuildEnv &env, long stride, long blklen, long oldsize, long extent, unsigned flags, bool pack);
Function* DLJIT_gen_blkidxfinal(BuildEnv &env, void *offsets, long blklen, long oldsize, long extent, unsigned flags, bool pack);
Function* DLJIT_gen_idxfinal(BuildEnv &env, void *offsets, void* blklens, long oldsize, long extent, unsigned flags, bool pack);

// Calculates the displacement in bytes for an element of the stack
#define disp_stack(idx, elem)                                           \
    ((idx)*sizeof(DLOOP_Dataloop_stackelm) + offsetof(DLOOP_Dataloop_stackelm, elem))

// Calculates the displacement in bytes for an element of the state
#define disp_state(elem)                   \
    offsetof(DLOOP_Dataloop_state, elem)   \

#endif
