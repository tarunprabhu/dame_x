DAME_TOOLCHAIN_DIR=$$HOME/research/datatypes/toolchain/install

CXX=g++
CXXFLAGS=-g -Wall -O3 -fPIC -std=c++11 # -DDEBUG
CXXINCLUDE=-I$(DAME_TOOLCHAIN_DIR)/include -I$(DAME_TOOLCHAIN_DIR)/../../mpich/install-dame-xed/include
CXXLIBS=-L$(DAME_TOOLCHAIN_DIR)/lib -lxed

TARGET=libdame-xed.so

default: $(TARGET)

$(TARGET): $(patsubst %.cpp, %.o, $(filter-out testdrv.cpp, $(wildcard *.cpp)))
	$(CXX) -shared -o $@ $^  $(CXXLIBS) -Wl,-rpath=$(DAME_TOOLCHAIN_DIR)/lib

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXINCLUDE) -o $@ $<

testdrv: testdrv.cpp $(TARGET)
	$(CXX) $(CXXFLAGS) $(CXXINCLUDE) -o $@ testdrv.cpp -L. -ldljit-mine -lxed

clean:
	rm -f *.o $(TARGET) testdrv
