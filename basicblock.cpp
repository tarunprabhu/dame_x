#include "basicblock.h"

#include "instruction.h"
#include "function.h"

#include <iomanip>
#include <iostream>

using namespace std;

BasicBlock::BasicBlock(std::string name, Function *fn, BasicBlock *before)
    : _name(name), addrset(false), _addr(0xbadf00d), _blockaddress(new BlockAddress(this)) {
    fn->insertBefore(this, before);
}

BasicBlock::~BasicBlock() {
    for(auto i : insts)
        delete i;
    delete _blockaddress;
}

void BasicBlock::append(Instruction* inst) { 
    inst->setParent(this);
    insts.push_back(inst); 
}

void BasicBlock::insertBefore(Instruction *n, Instruction *loc) {
    n->setParent(this);
    unsigned i;
    for(i = 0; insts[i] != loc and i < insts.size(); i++)
        ;
    insts[i] = n;
}

bool BasicBlock::pass1(unsigned &curr) {
    _addr = curr;
    addrset = true;
    for(auto i : insts)
        if(not i->pass1(curr)) 
            return false;
    return true;
}

bool BasicBlock::pass2(vector<byte> &buf, unsigned &pos) {
    for(auto i : insts) 
        if(not i->pass2(buf, pos))
            return false;
    return true;
}

void BasicBlock::gas(ostream &os, unsigned &idx, int opt) const {
    if(opt == 0) {
        os << right << setfill('0') << setw(16) << hex << _addr;
        os << " <" << _name << ">: " << endl;
    } else {
        os << _name << ":" << endl;
    }
    for(auto i : insts)
        i->gas(os, idx, opt);
}
