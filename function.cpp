#include "function.h"

#include "basicblock.h"
#include "values.h"

#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

Function::~Function() {
    for(auto bb : blocks)
        delete bb;
}

void Function::insertBefore(BasicBlock *bb, BasicBlock *pos) {
    bb->setParent(this);
    if(not pos) {
        blocks.push_back(bb);
    } else {
        vector<BasicBlock*>::iterator it;
        for(it = blocks.begin(); it != blocks.end(); it++)
            if(*it == pos)
                break;
        blocks.insert(it, bb);
    }
}

int Function::indexOf(const BasicBlock* bb) const {
    for(unsigned i = 0; i < blocks.size(); i++)
        if(blocks[i] == bb)
            return i;
    return -1;
}

bool Function::jtadd(const BasicBlock *bb, unsigned idx) {
    jumptable[idx] = bb;
    return true;
}

unsigned Function::jtexpand(unsigned count) {
    unsigned pos = jumptable.size();
    jumptable.resize(jumptable.size()+count);
    return pos;
}

void Function::copy(byte *ptr) const {
    uint64_t *offsets = (uint64_t*)ptr;
    for(unsigned i = 0; i < jumptable.size(); i++)
        offsets[i] = (uint64_t)(jumptable.at(i)->addr());

    byte *b = ptr;
    unsigned start = jumptable.size()*sizeof(*offsets);
    for(unsigned i = start; i < opcodes.size(); i++)
        b[i] = opcodes.at(i);
}

bool Function::pass1(unsigned &curr) {
    pos = curr;
    for(auto bb : blocks)
        if(not bb->pass1(curr))
            return false;
    return true;
}

bool Function::pass2(vector<byte> &opcodes, unsigned &pos) {
    for(auto bb : blocks)
        if(not bb->pass2(opcodes, pos))
            return false;
    return true;
}

bool Function::encode() {
    if(opcodes.size())
        return true;

    // Build order in which to process functions. We're not going to have
    // recursion in datatypes (I hope!)
    for(auto f : callees)
        f->constructBuildOrder(_buildorder);
    _buildorder.push_back(this);

    unsigned curr = jumptable.size()*sizeof(uint64_t);
    for(auto f : _buildorder)
        if(not f->pass1(curr))
            return false;

    opcodes = vector<byte>(curr);

    curr = jumptable.size()*sizeof(uint64_t);
    for(auto f : _buildorder)
        if(not f->pass2(opcodes, curr))
            return false;

    return true;
}

void Function::gas(ostream &os, unsigned &idx, int opt) const { 
    os << "/* " << "function: " << _name << " */" << endl;
    if(jumptable.size()) {
        os << "/*" << endl;
        os << "        " << "jumptable" << endl << endl;
        for(unsigned i = 0; i < jumptable.size(); i++) {
            os << right << setw(10) << i << "    ";
            os << left << setw(20) << jumptable.at(i)->name();
            os << left << jumptable.at(i)->addr() << endl;
        }
        os << "*/" << endl;
    }
    for(auto bb : blocks)
        bb->gas(os, idx, opt);
}

void Function::printPtr(ostream &os, byte *base) const {
    os << left << setw(50) << name() << hex << (void*)(base+entry()) << endl;
}

void Function::constructBuildOrder(std::vector<Function*> &order) {
    for(auto f : callees)
        f->constructBuildOrder(order);
    order.push_back(this);
}



LibraryFunction::LibraryFunction(std::string name, void *ptr)
    : Function(name) {
    _addr = new Constant((uint64_t)ptr, 64);
}

LibraryFunction::~LibraryFunction()  {
    delete _addr;
}

bool LibraryFunction::pass1(unsigned &pos) {
    return true;
}

bool LibraryFunction::pass2(vector<byte> &buf, unsigned& pos) {
    return true;
}

bool LibraryFunction::encode() {
    return true;
}

void LibraryFunction::gas(std::ostream &os) const { 
    os << "/* " << _name << " */" << endl;
}

void LibraryFunction::printPtr(ostream &os, byte *base) const {
    os << left << setw(50) << name() << hex << _addr->val() << endl;
}
