#include "buildenv.h"

#include "function.h"
#include "basicblock.h"
#include "instruction.h"

using namespace std;

BuildEnv::BuildEnv(const Dataloop *dl, Module &module, Function *f)
    : _dl(dl), 
      _builder(Builder()),
      _module(module),
      _function(f),
      _bbTail(nullptr),
      consts64(decltype(consts64)()), consts32(decltype(consts32)()), consts8(decltype(consts8)()) {

    _paramState = _builder.StackVar(8);
    _paramSize = _builder.StackVar(8);
    _paramCopied = _builder.StackVar(8);
    _retval = _builder.StackVar(8);
    _jtaddr = _builder.StackVar(8);
    _resumeAddr = _builder.StackVar(8);
}

BuildEnv::~BuildEnv() {
    for(auto c : consts64) { delete c.second; }
    for(auto c : consts32) { delete c.second; }
    for(auto c : consts8)  { delete c.second; }
}

const Constant* BuildEnv::int64Const(int64_t val, bool sign) {
    auto c = consts64.find(val);
    if(c != consts64.end())
        return c->second;
    consts64[val] = new Constant(val, 64, sign);
    return consts64.at(val);
}

const Constant* BuildEnv::int32Const(int32_t val, bool sign) {
    auto c = consts32.find(val);
    if(c != consts32.end())
        return c->second;
    consts32[val] = new Constant(val, 32, sign);
    return consts32.at(val);
}

const Constant* BuildEnv::int8Const(int8_t val, bool sign) {
    auto c = consts8.find(val);
    if(c != consts8.end())
        return c->second;
    consts8[val] = new Constant(val, 8, sign);
    return consts8.at(val);
}
