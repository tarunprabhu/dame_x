#ifndef DLJIT_DATALOOP_H
#define DLJIT_DATALOOP_H

/* These have been copied from dataloop_parts.h in src/mpid/common/datatypes/dataloop.
 * Probably should figure out a way to include that file directly, 
 * but it's probably not worth the trouble */

#define PREPEND_PREFIX(x) x

// Copy corresponding code from here onwards from dataloop_parts.h

#define DLOOP_Dataloop              PREPEND_PREFIX(Dataloop)
#define DLOOP_Dataloop_contig       PREPEND_PREFIX(Dataloop_contig)
#define DLOOP_Dataloop_vector       PREPEND_PREFIX(Dataloop_vector)
#define DLOOP_Dataloop_blockindexed PREPEND_PREFIX(Dataloop_blockindexed)
#define DLOOP_Dataloop_indexed      PREPEND_PREFIX(Dataloop_indexed)
#define DLOOP_Dataloop_struct       PREPEND_PREFIX(Dataloop_struct)
#define DLOOP_Dataloop_stackelm     PREPEND_PREFIX(Dataloop_stackelm)

typedef enum { 
    DL_BOTTOM          = -1, 
    DL_EXIT            = 0,
    
    DL_CONTIG          = 1,   
    DL_CONTIGFINAL     = 11,

    DL_VECTOR          = 2,   
    DL_VECTOR1         = 3, 
    DL_VECTORFINAL     = 21,
    DL_VECFINAL_1      = 22, 
    DL_VECFINAL_2      = 23, 
    DL_VECFINAL_4      = 24, 
    DL_VECFINAL_8      = 25,
    
    DL_BLOCKINDEX      = 4, 
    DL_BLOCKINDEX1     = 5,
    DL_BLOCKINDEXFINAL = 41,
    DL_BLKINDEXFINAL_1 = 42,
    DL_BLKINDEXFINAL_2 = 43,
    DL_BLKINDEXFINAL_4 = 44,
    DL_BLKINDEXFINAL_8 = 45,

    DL_INDEX           = 6,
    DL_INDEXFINAL      = 61,

    DL_STRUCT          = 7,

    DL_CONTIGCHILD     = 8, 

    DL_RETURNTO        = 31
} DLOOP_Dataloop_kind;

// Forward declaration
struct DLOOP_Dataloop;

typedef struct DLOOP_Dataloop_contig {
    MPI_Aint basesize;
    MPI_Aint baseextent;
} DLOOP_Dataloop_contig;

typedef struct DLOOP_Dataloop_vector { 
    MPI_Aint oldsize;
    MPI_Aint blklen;  /* This is the blocklength in #elements */
    MPI_Aint stride;
} DLOOP_Dataloop_vector;

typedef struct DLOOP_Dataloop_blockindexed {
    MPI_Aint oldsize;
    MPI_Aint blklen;
    const MPI_Aint *offsets;
} DLOOP_Dataloop_blockindexed;

typedef struct DLOOP_Dataloop_indexed {
    MPI_Aint oldsize;
    const MPI_Aint *blklens; 
    const MPI_Aint *offsets;
} DLOOP_Dataloop_indexed;

typedef struct DLOOP_Dataloop_struct {
    const MPI_Aint *oldsizes;
    const MPI_Aint *blklens;
    const MPI_Aint *offsets;
    struct DLOOP_Dataloop **dls;
} DLOOP_Dataloop_struct;

typedef struct DLOOP_Dataloop {
    DLOOP_Dataloop_kind kind;
    int          count;
    MPI_Aint     size;
    MPI_Aint     extent;
    /* This is used when operating on a struct type. It indicates where in the 
     * stack the control should return */
    int          returnto;
    /* Additional information about the dataloop, potentially to enable 
     * optimizations */
    unsigned     flags;
    union {
        DLOOP_Dataloop_contig       c_t;
        DLOOP_Dataloop_vector       v_t;
        DLOOP_Dataloop_blockindexed bi_t;
        DLOOP_Dataloop_indexed      i_t;
        DLOOP_Dataloop_struct       s_t;
    } s;
} DLOOP_Dataloop;

/* The max datatype depth is the maximum depth of the stack used to 
   evaluate datatypes.  It represents the length of the chain of 
   datatype dependencies.  Defining this and testing when a datatype
   is created removes a test in the datatype evaluation loop. */
#define DLOOP_MAX_DATATYPE_DEPTH 64
#define DLOOP_MAX_DATALOOP_STACK DLOOP_MAX_DATATYPE_DEPTH
typedef struct DLOOP_Dataloop_stackelm {
    /* This is the base address used to keep track of 
     * the src pointer when packing and the dst pointer when unpacking */
    MPI_Offset base;
    /* When we encounter a struct type, we push the current dataloop here
     * and use the dataloop of the struct element being processed */
    DLOOP_Dataloop *prevdl;
    /* For the *FINAL types, countLeft will be measured in bytes because 
     * it's the #bytes that remain to be copied.
     * For all others, it will be in #elements */
    MPI_Aint countLeft;
} DLOOP_Dataloop_stackelm;

typedef struct DLOOP_Dataloop_state {
    long sp;
    DLOOP_Dataloop *dl;
    MPI_Aint partial;
    DLOOP_Dataloop_stackelm stack[DLOOP_MAX_DATALOOP_STACK];
} DLOOP_Dataloop_state;


/* Optimization flags */
#define DLOOP_OPT_FLAG_ALIGNED 0x1
#define DLOOP_OPT_FLAG_ISSHORT 0x2

/* When copying blocks larger than this, use memcpy. 
 * TODO: Provide a way of configuring this useing ./configure */
#define DLOOP_MEMCPY_THRESHOLD 1024

/* Helper macros */
#define DLOOP_Dataloop_is_contig(dl)                    \
    ((dl)[1].kind == DL_CONTIGFINAL)                    \
    
#define set_dlflag(fvec, f)                   \
    ((fvec) | ~(f))                           \
    
#define get_dlflag(fvec, f)                   \
    ((fvec) & (f))                            \
    
#define DLOOP_opt_get_aligned(dl) get_dlflag((dl).flags, DLOOP_OPT_FLAG_ALIGNED)
#define DLOOP_opt_set_aligned(dl) set_dlflag((dl).flags, DLOOP_OPT_FLAG_ALIGNED)

#define DLOOP_opt_get_isshort(dl) get_dlflag((dl).flags, DLOOP_OPT_FLAG_ISSHORT)
#define DLOOP_opt_set_isshort(dl) set_dlflag((dl).flags, DLOOP_OPT_FLAG_ISSHORT)

// End of code copied from MPICH 

#define flag_aligned(flags) get_dlflag((flags), DLOOP_OPT_FLAG_ALIGNED)
#define flag_isshort(flags) get_dlflag((flags), DLOOP_OPT_FLAG_ISSHORT)

#endif

