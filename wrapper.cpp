#include "wrapper.h"

#include "module.h"
#include "util.h"

#include <cstdlib>
#include <cstring>
#include <iomanip>

using namespace std;

DLJIT_wrapper::DLJIT_wrapper(Module &module) {
    Function *fnPack = module.getFunction("DL_pack");
    pack = (byte*)DLJIT_malloc_exec(fnPack->size());
    fnPack->copy(pack);
    packEntry = (DLJIT_function_ptr)(pack + fnPack->entry());

    Function *fnUnpack = module.getFunction("DL_unpack");
    unpack = (byte*)DLJIT_malloc_exec(fnUnpack->size());
    fnUnpack->copy(unpack);
    unpackEntry = (DLJIT_function_ptr)(unpack + fnUnpack->entry());
}

DLJIT_wrapper::~DLJIT_wrapper() {
    free(pack);
    free(unpack);
}

DLJIT_function_ptr DLJIT_wrapper::getPack() const {
    return packEntry;
}

DLJIT_function_ptr DLJIT_wrapper::getUnpack() const {
    return unpackEntry;
}
