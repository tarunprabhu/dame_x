#include "jit.h"

#include "util.h"

#include <iostream>

using namespace std;

static bool DLJIT_build_header(BuildEnv &env, BasicBlock *bbEntry, BasicBlock *bbNotResume, bool pack) {
    Function *fn = env.fn();
    Builder& builder = env.builder();

    BasicBlock *bbResume = new BasicBlock("resume", fn, bbNotResume);
    
    builder.SetInsertPoint(bbEntry);
    DLJIT_gen_function_header(env, bbEntry);

    const Register *paramIn = builder.argReg(PARAM_IN);
    const Register *paramOut = builder.argReg(PARAM_OUT);
    const Register *paramSize = builder.argReg(PARAM_SIZE);
    const Register *paramState = builder.argReg(PARAM_STATE);
    const Register *paramCopied = builder.argReg(PARAM_COPIED);
    
    // Allocate stack variables. These have been declared in the BuildEnv
    // constructor. This comes here because all that temporary junk while
    // subtracting clobbers registers
    builder.StackAllocate();

    // Save some of the arguments to stack
    builder.CreateStore(paramSize, builder.sp(), env.paramSize());
    builder.CreateStore(paramState, builder.sp(), env.paramState());
    builder.CreateStore(paramCopied, builder.sp(), env.paramCopied());

    builder.CreateMove(env.inbase(), paramIn);
    builder.CreateMove(env.outbase(), paramOut);
    builder.CreateMove(env.sizeleft(), paramSize);
    builder.CreatePtr(env.stack(), paramState, disp_state(stack));
    builder.CreateLoad(env.partial(), paramState, disp_state(partial));

    const Register *tmp = builder.getRegister();
    const Register *tmpbbaddr = builder.getRegister();
    builder.CreatePtr(tmp, builder.rip());
    builder.CreateMove(tmpbbaddr, bbEntry->blockaddress());
    builder.CreateSub(tmp, tmp, tmpbbaddr);
    // FIXME: I shouldn't hardcode the offset of the current instruction.
    // It should be safe because the only variable length instructions are the
    // number of registers whose values are saved.
    builder.CreateSub(tmp, tmp, env.int64Const(57));
    builder.CreateStore(tmp, builder.sp(), env.jtaddr());
    builder.clearRegister(tmpbbaddr);
    builder.clearRegister(tmp);
    
    const Register *state = builder.getRegister(); // Just in case
    const Register *sp = builder.getRegister();
    builder.CreateLoad(state, builder.sp(), env.paramState());
    builder.CreateStore(env.int64Const(0), builder.sp(), env.retval());
    builder.CreateLoad(sp, state, disp_state(sp));
    builder.CreateCondBr(builder.CreateICmpNZ(sp), bbResume, bbNotResume);

    builder.SetInsertPoint(bbResume);
    const Register *resumeAddr = builder.getRegister();
    builder.CreateMul(sp, sp, env.int64Const(sizeof(DLOOP_Dataloop_stackelm)));
    if(pack) {
        builder.CreateLoad(env.inbase(), env.stack(), offsetof(DLOOP_Dataloop_stackelm, base), sp);
    } else {
        builder.CreateLoad(env.outbase(), env.stack(), offsetof(DLOOP_Dataloop_stackelm, base), sp);
    }
#ifdef DEBUG
    DEBUG_PUTS(env, "resume");
#endif    
    builder.CreateLoad(resumeAddr, state, disp_state(dl));
    builder.CreateIndirectBr(resumeAddr);
    builder.CreateBr(bbNotResume);

    builder.clearRegister(resumeAddr);
    builder.clearRegister(sp);
    builder.clearRegister(state);
    
    return true;
}

static bool DLJIT_build_tail(BuildEnv &env, BasicBlock *bbExit) {
    Builder& builder = env.builder();

    builder.SetInsertPoint(bbExit);
    builder.CreateLoad(builder.ret(), builder.sp(), env.retval());
    builder.StackDeallocate();
    DLJIT_gen_function_footer(env, bbExit);
    return true;
}

static bool DLJIT_build_function(BuildEnv &env, bool pack) {
    Function *fn = env.fn();
    Builder &builder = env.builder();

    builder.StartFunction();
#ifdef DEBUG
    builder.startCheck(fn->name());
#endif

    env.setInbase(builder.getRegister(1));
    env.setOutbase(builder.getRegister(1));
    env.setSizeleft(builder.getRegister(1));
    env.setStack(builder.getRegister(1));
    env.setPartial(builder.getRegister(1));

    BasicBlock *bbTail = new BasicBlock("tail", fn);
    BasicBlock *bbExit = new BasicBlock("exit", fn, bbTail);
    BasicBlock *bbEntry = new BasicBlock("entry", fn, bbExit);
    env.setBBTail(bbTail);

    BasicBlock *bb = DLJIT_gen_loop(env, 1, bbExit, bbExit, pack);
    if(not bb)
        return false;
    if(not DLJIT_build_header(env, bbEntry, bb, pack))
        return false;

    // Exit block
    builder.SetInsertPoint(bbExit);
    const Register *state = builder.getRegister();
    const Register *size = builder.getRegister();
    const Register *copied = builder.getRegister();
    const Register *tmp = builder.getRegister();
    
    builder.CreateLoad(state, builder.sp(), env.paramState());
    builder.CreateLoad(size, builder.sp(), env.paramSize());
    builder.CreateLoad(copied, builder.sp(), env.paramCopied());
    builder.CreateStore(env.int64Const(0), state, disp_state(sp));
    builder.CreateStore(env.int64Const(0), state, disp_state(dl));
    builder.CreateStore(env.partial(), state, disp_state(partial));
    builder.CreateSub(tmp, size, env.sizeleft());
    builder.CreateStore(tmp, copied);

    builder.SetInsertPoint(bbTail);
    DLJIT_build_tail(env, bbTail);
    
    builder.clearRegister(tmp);
    builder.clearRegister(copied);
    builder.clearRegister(size);
    builder.clearRegister(state);

#ifdef DEBUG
    builder.endCheck(fn->name());
#endif
    builder.EndFunction();
    
    return true;
}

DLJIT_wrapper* DLJIT_compile_all(Dataloop *dl) {
    Module module;

    Function *fnP = module.getOrInsertFunction("DL_pack");
    BuildEnv envP(dl, module, fnP);
    if(not(DLJIT_build_function(envP, true) and fnP->encode()))
        return nullptr;

    Function *fnU = module.getOrInsertFunction("DL_unpack");
    BuildEnv envU(dl, module, fnU);
    if(not (DLJIT_build_function(envU, false) and fnU->encode()))
        return nullptr;

    return new DLJIT_wrapper(module);
}

BasicBlock* DLJIT_gen_loop(BuildEnv &env, int sp, BasicBlock* bbPrev, BasicBlock *bbExit, bool pack) {
    switch(env.dl()[sp].kind) {
    case DL_CONTIG:
        return DLJIT_contiguous(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_VECTOR:
    case DL_VECTOR1:
        return DLJIT_vector(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_BLOCKINDEX:
    case DL_BLOCKINDEX1:
        return DLJIT_blockindexed(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_INDEX:
        return DLJIT_indexed(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_STRUCT:
        return DLJIT_struct(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_VECTORFINAL:
        return DLJIT_vectorfinal(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_BLOCKINDEXFINAL:
        return DLJIT_blockindexedfinal(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_INDEXFINAL:
        return DLJIT_indexedfinal(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_CONTIGCHILD:
        return DLJIT_contigchild(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_CONTIGFINAL:
        return DLJIT_contigfinal(env, sp, bbPrev, bbExit, pack);
        break;
    case DL_RETURNTO:
        assert(0 && "Shouldn't be here");
    case DL_BOTTOM:
    case DL_EXIT:
    default:
        return nullptr;
        break;
    }
    return nullptr;
}
